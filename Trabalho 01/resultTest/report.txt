Relat�rio do Compilador

53 de um total de 139 erros que deveriam ser sinalizados n�o o foram (38%)
16 erros foram sinalizados na linha errada (11%)
12 erros foram sinalizados em 79 arquivos sem erro (15%)

Erros que deveriam ser sinalizados mas n�o foram:

ER-SEM60.KRA, 21, Method 'p' was not found in the public interface of 'A' or its superclasses (comp.Compiler.factor())

ER-SEM80.KRA, 7, Method 'run' of class 'Program' with a return value type different from 'void' (comp.Compiler.methodDec())

ER-SEM42.KRA, 14, Type error: type of the left-hand side of the assignment is a basic type and the type of the right-hand side is a class (comp.Compiler.assignExprLocalDec())

ER-SEM37.KRA, 20, Method 'set' was not found in class 'A' or its superclasses (comp.Compiler.factor())

ER-SEM47.KRA, 24, Method 'm' was not found in superclass 'B' or its superclasses (comp.Compiler.factor())

ER-SEM11.KRA, 10, non-boolean expression in 'while' command (comp.Compiler.whileStatement())

ER-SEM50.KRA, 17, Command 'break' outside a command 'while' (comp.Compiler.breakStatement())

ER-SEM36.KRA, 20, Expression expected in the right-hand side of assignment (comp.Compiler.assignExprLocalDec())

ER-SEM86.KRA, 8, Class 'A' was not found (comp.Compiler.factor())

ER-SEM15.KRA, 9, Operator '!' does not accepts 'int' values (comp.Compiler.factor())

ER-SEM79.KRA, 5, Method 'run' of class 'Program' cannot take parameters

ER-SEM41.KRA, 14, Type error: the type of the expression of the right-hand side is a basic type and the type of the variable of the left-hand side is a class (comp.Compiler.assignExprLocalDec())

ER-SEM02.KRA, 9, Variable 'I' was not declared (comp.Compiler.assignExprLocalDec())

ER-SEM03.KRA, 10, Variable 'i' is being redeclared (comp.Compiler.localDec())

ER-SEM78.KRA, 10, Source code without a class 'Program' (comp.Compiler.program())

ER-SEM68.KRA, 12, Method 'p' was not found in class 'Program' or its superclasses (comp.Compiler.factor())

ER-SEM57.KRA, 21, Incompatible types cannot be compared with '==' because the result will always be 'false' (comp.Compiler.expr())

ER-SEM33.KRA, 11, Method 'm' is being redeclared (comp.Compiler.methodDec())

ER-SEM39.KRA, 16, Type error: type of the expression returned is not subclass of the method return type (comp.Compiler.returnStatement())

ER-SIN17.KRA, 7, Missing ';' (comp.Compiler.localDec())

ER-SEM88.KRA, 13, boolean expression expected in a do-while statement (comp.Compiler.doWhileStatement())

ER-SEM28.KRA, 10, Variable 'i' is being redeclared (comp.Compiler.localDec())

ER-SEM14.KRA, 12, Command 'write' does not accept 'boolean' expressions (comp.Compiler.writeStatement())

ER-SEM07.KRA, 10, Message send to a non-object receiver (comp.Compiler.factor())

ER-SEM70.KRA, 10, Method 'm' is being redefined (comp.Compiler.methodDec())

ER-SEM38.KRA, 20, Type error: type of the right-hand side of the assignment is not a subclass of the left-hand side (comp.Compiler.assignExprLocalDec())

ER-SEM34.KRA, 19, Message send 'a.m()' returns a value that is not used (comp.Compiler.statement())

ER-SEM13.KRA, 11, Command 'read' does not accept 'boolean' variables (comp.Compiler.readStatement())

ER-SEM26.KRA, 9, 'break' statement found outside a 'while' statement (comp.Compiler.statement()))

ER-SEM12.KRA, 9, operator '+' of 'int' expects an 'int' value (comp.Compiler.simpleExpr())

ER-SEM32.KRA, 11, Method 'm' is being redeclared (comp.Compiler.methodDec())

ER-SEM30.KRA, 16, Method 'put' of subclass 'B' has a signature different from method inherited from superclass 'A' (comp.Compiler.methodDec())

ER-SEM09.KRA, 10, type 'int' does not support operator '&&' (comp.Compiler.term())

ER-SIN04.KRA, 8, Statement expected (comp.Compiler.assignExprLocalDec())

ER-SEM40.KRA, 40, Type error: the type of the real parameter is not subclass of the type of the formal parameter (comp.Compiler.factor())

ER-SEM43.KRA, 10, Type error: 'null' cannot be assigned to a variable of a basic type (comp.Compiler.assignExprLocalDec())

ER-SEM89.KRA, 13, boolean expression expected in a do-while statement (comp.Compiler.doWhileStatement())

ER-SEM61.KRA, 33, Method 's' was not found in class 'C' or its superclasses (comp.Compiler.factor())

ER-SEM58.KRA, 21, Incompatible types cannot be compared with '!=' because the result will always be 'false' (comp.Compiler.expr())

ER-SEM77.KRA, 10, Method 'run' was not found in class 'Program' (comp.Compiler.classDec())

ER-SEM59.KRA, 25, Method 'p' was not found in the public interface of 'A' or its superclasses (comp.Compiler.factor())

ER-SEM31.KRA, 10, Method 'i' has name equal to an instance variable (comp.Compiler.methodDec())

ER-SEM51.KRA, 17, Method 'put' is being redefined in subclass 'B' with a signature different from the method of superclass 'A' (comp.Compiler.methodDec())

ER-SEM62.KRA, 15, Identifier 'a' was not found (comp.Compiler.factor())

ER-SEM05.KRA, 10, 'int' cannot be assigned to 'boolean' (comp.Compiler.assignExprLocalDec())

ER-SEM44.KRA, 26, Command 'write' does not accept objects (comp.Compiler.writeStatement())

ER-SEM01.KRA, 16, Missing 'return' statement in method 'm' (comp.Compiler.methodDec())

ER-SEM81.KRA, 7, Method 'run' of class 'Program' cannot be private (comp.Compiler.methodDec())

ER-SEM27.KRA, 6, Class 'A' is inheriting from itself (comp.Compiler.classDec())

ER-SEM08.KRA, 8, type boolean does not support operation '+' (comp.Compiler.simpleExpr())

ER-SEM04.KRA, 11, Type error: value of the right-hand side is not subtype of the variable of the left-hand side. (comp.Compiler.assignExprLocalDec())

ER-SEM35.KRA, 9, Illegal 'return' statement. Method returns 'void' (comp.Compiler.returnStatement())

ER-SEM46.KRA, 9, 'super' used in class 'Program' that does not have a superclass (comp.Compiler.factor())

######################################################
Erros que foram sinalizados na linha errada:

ER-SEM69.KRA
    correto:    11, '.' or '=' expected after identifier OR statement expected (comp.Compiler.factor())
    sinalizado: 7, public/private or "}" expected

ER-SEM84.KRA
    correto:    12, Redeclaration of final method 'finalMethod' (comp.Compiler.methodDec())
    sinalizado: 6, public/private or "}" expected

ER-SEM66.KRA
    correto:    9, Static method 'p' was not found in class 'Program' (comp.Compiler.factor())
    sinalizado: 8, public/private or "}" expected

ER-SEM72.KRA
    correto:    11, Call to 'this' in a static method (comp.Compiler.factor())
    sinalizado: 10, public/private or "}" expected

ER-SEM83.KRA
    correto:    7, Class 'Program' is inheriting from final class 'Terra' 
    sinalizado: 3, 'class' expected

ER-SEM75.KRA
    correto:    14, Static method 'm' was not found in class 'A' (comp.Compiler.factor())
    sinalizado: 6, public/private or "}" expected

ER-SEM67.KRA
    correto:    12, '.' or '=' expected after an identifier OR statement expected (comp.Compiler.factor())
    sinalizado: 11, public/private or "}" expected

ER-SEM85.KRA
    correto:    6, 'final' method in a 'final' class (comp.Compiler.methodDec())
    sinalizado: 5, 'class' expected

ER-SEM65.KRA
    correto:    9, Static method 'p' was not found in class 'Program' (comp.Compiler.factor())
    sinalizado: 8, public/private or "}" expected

ER-SEM64.KRA
    correto:    11, '.' or '=' expected after an identifier OR statement expected (comp.Compiler.factor())
    sinalizado: 7, public/private or "}" expected

ER-SEM71.KRA
    correto:    9, Attempt to access an instance variable using 'this' in a static method (comp.Compiler.factor())
    sinalizado: 8, public/private or "}" expected

ER-SEM74.KRA
    correto:    12, '.' or '=' expected after identifier OR statement expected (comp.Compiler.factor())
    sinalizado: 6, public/private or "}" expected

ER-SEM76.KRA
    correto:    16, Method 'm' was not found in class 'A' or its superclasses (comp.Compiler.factor())
    sinalizado: 6, public/private or "}" expected

ER-SEM63.KRA
    correto:    12, '.' or '=' expected after an identifier OR statement expected (comp.Compiler.factor())
    sinalizado: 8, public/private or "}" expected

ER-SEM73.KRA
    correto:    10, Redefinition of static method 'm' (comp.Compiler.methodDec())
    sinalizado: 7, public/private or "}" expected

ER-SEM87.KRA
    correto:    11, Call to 'this' in a static method (comp.Compiler.factor())
    sinalizado: 10, public/private or "}" expected

######################################################
Erros que foram sinalizados mas n�o deveriam ter sido:

OK-SEM17.KRA, 11, public/private or "}" expected

OK-SEM18.KRA, 11, public/private or "}" expected

OK-SIN16.KRA, 26, Statement expected

OK-GER19.KRA, 11, public/private or "}" expected

OK-SEM01.KRA, 4, public/private or "}" expected

OK-GER18.KRA, 11, public/private or "}" expected

OK-SEM20.KRA, 6, public/private or "}" expected

OK-GER20.KRA, 12, public/private or "}" expected

OK-SEM19.KRA, 12, public/private or "}" expected

OK-GER22.KRA, 14, public/private or "}" expected

OK-GER23.KRA, 24, Statement expected

OK-GER17.KRA, 11, public/private or "}" expected

######################################################
Em todos os testes abaixo, o compilador sinalizou o erro na linha correta (quando o teste tinha erros) ou n�o sinalizou o erro (quando o teste N�O tinha erros). Mas � necess�rio conferir se as mensagens emitidas pelo compilador s�o compat�veis com as mensagens de erro sugeridas pelas chamadas aos metaobjetos dos testes. 

A lista abaixo cont�m o nome do arquivo de teste, a mensagem que ele sinalizou e a mensagem sugerida pelo arquivo de teste

ER-SIN55.KRA
The compiler message was: "Class 'static' not declared"
The 'ce' message is:      "Identifier expected"


ER-SIN11.KRA
The compiler message was: "( expected"
The 'ce' message is:      "Missing '('"


ER-SIN37.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Expression expected"


ER-SIN44.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'public', 'private', or '}' expected"


ER-SIN57.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'public', 'private' or '}' expected"


ER-SIN06.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Expression expected"


ER-SIN22.KRA
The compiler message was: "';' expected"
The 'ce' message is:      "Missing ';'"


ER-SEM18.KRA
The compiler message was: "Type 'k' was not found"
The 'ce' message is:      "Type 'k' was not found"


ER-SIN09.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected"


ER-SIN39.KRA
The compiler message was: "{ expected"
The 'ce' message is:      "'{' expected"


ER-SEM23.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN21.KRA
The compiler message was: "';' expected"
The 'ce' message is:      "Missing ';'"


ER-SIN43.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'public', 'private', or '}' expected"


ER-SIN32.KRA
The compiler message was: "Type expected"
The 'ce' message is:      "Type expected"


ER-SIN36.KRA
The compiler message was: "Class 'm' not declared"
The 'ce' message is:      "Identifier expected"


ER-LEX05.KRA
The compiler message was: "Invalid Character: '#'"
The 'ce' message is:      "Unknown character"


ER-SIN53.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'private' expected"


ER-SIN13.KRA
The compiler message was: "{ expected"
The 'ce' message is:      "'{' expected"


ER-SIN60.KRA
The compiler message was: "( expected"
The 'ce' message is:      "'(' expected after 'while'"


ER-SEM49.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Class expected"


ER-LEX09.KRA
The compiler message was: "Number out of limits"
The 'ce' message is:      "literal int out of limits"


ER-SIN34.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected"


ER-SIN25.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Expression expected OR invalid sequence of symbols"


ER-SIN08.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Command 'write' without arguments"


ER-LEX03.KRA
The compiler message was: "Number out of limits"
The 'ce' message is:      "literal int out of limits"


ER-SIN58.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected"


ER-SIN19.KRA
The compiler message was: "; expected"
The 'ce' message is:      "Missing ';'"


ER-SIN01.KRA
The compiler message was: "{ expected"
The 'ce' message is:      "'{' expected"


ER-SEM17.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Command 'read' expects a variable"


ER-SEM19.KRA
The compiler message was: "Type 'Program' was not found"
The 'ce' message is:      "Type 'Program' was not found"


ER-SEM82.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "Method 'run' cannot be static"


ER-SIN02.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Missing identifier"


ER-SIN40.KRA
The compiler message was: "Attempt to declare a public instance variable"
The 'ce' message is:      "Attempt to declare public instance variable 'i'"


ER-SIN14.KRA
The compiler message was: "End of file expected"
The 'ce' message is:      "'class' expected"


ER-SIN23.KRA
The compiler message was: "} expected"
The 'ce' message is:      "Statement expected"


ER-SIN38.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'}' expected"


ER-SEM21.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN18.KRA
The compiler message was: "';' expected"
The 'ce' message is:      "Missing ';'"


ER-SIN28.KRA
The compiler message was: "'class' expected"
The 'ce' message is:      "'class' expected"


ER-SIN30.KRA
The compiler message was: "Invalid Character: '#'"
The 'ce' message is:      "Unknown character '#'"


ER-SIN07.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Command 'read' without arguments"


ER-LEX02.KRA
The compiler message was: "Invalid Character: '\'"
The 'ce' message is:      "Unknown character '\'"


ER-SIN41.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-LEX04.KRA
The compiler message was: "Number out of limits"
The 'ce' message is:      "literal int out of limits"


ER-LEX06.KRA
The compiler message was: "'_' cannot start an indentifier"
The 'ce' message is:      "Identifier starting with underscore"


ER-SIN52.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'public' or 'private' expected"


ER-SIN05.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Expression expected"


ER-SIN03.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SEM20.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-LEX07.KRA
The compiler message was: "Statement expected"
The 'ce' message is:      "Unknown character"


ER-SEM24.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN15.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected"


ER-SIN54.KRA
The compiler message was: "Class 'static' not declared"
The 'ce' message is:      "Identifier expected"


ER-SIN12.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Expression expected"


ER-SIN26.KRA
The compiler message was: ") expected"
The 'ce' message is:      "Expression expected OR invalid sequence of symbols"


ER-SIN59.KRA
The compiler message was: "Type 'do' was not found"
The 'ce' message is:      "'{' expected after 'do'"


ER-SIN29.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'private',  'public', or '}' expected"


ER-SIN33.KRA
The compiler message was: "Class 'x' not declared"
The 'ce' message is:      "Identifier expected"


ER-SEM48.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN10.KRA
The compiler message was: "( expected"
The 'ce' message is:      "'(' expected after 'read' command"


ER-SIN31.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "'public', 'private', or '}' expected"


ER-SIN56.KRA
The compiler message was: "public/private or "}" expected"
The 'ce' message is:      "Identifier expected"


ER-SIN16.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN51.KRA
The compiler message was: "Class 'static' not declared"
The 'ce' message is:      "Identifier expected"


ER-SEM25.KRA
The compiler message was: "Identifier expected"
The 'ce' message is:      "Identifier expected"


ER-SIN35.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected"


ER-SEM06.KRA
The compiler message was: "Statement expected"
The 'ce' message is:      "'operator expected' or 'variable expected at the left-hand side of a assignment'"


ER-SIN27.KRA
The compiler message was: ") expected"
The 'ce' message is:      "')' expected OR Unknown sequence of symbols"


ER-SIN24.KRA
The compiler message was: "Expression expected"
The 'ce' message is:      "Expression expected OR Unknown sequence of symbols"


ER-SIN20.KRA
The compiler message was: "; expected"
The 'ce' message is:      "Missing ';'"


