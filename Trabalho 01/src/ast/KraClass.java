package ast;
/*
 * Krakatoa Class
 */
public class KraClass extends Type {
	
	   public KraClass( String name ) {
	      super(name);
	   }
	   
	   public String getCname() {
	      return getName();
	   }
	   
	   public KraClass getSuperclass() {
		   return superclass;
	   }

	   public void setSuperclass(KraClass superclass) {
		   this.superclass = superclass;
	   }

	   private String name;
	   private KraClass superclass;
	   private InstanceVariableList instanceVariableList;
	   // private MethodList publicMethodList, privateMethodList;
	   // métodos públicos get e set para obter e iniciar as variáveis acima,
	   // entre outros métodos
}