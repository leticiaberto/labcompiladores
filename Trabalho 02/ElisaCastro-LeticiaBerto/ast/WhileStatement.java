/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class WhileStatement extends Statement{

	@Override
	public void genC(PW pw) {		
		pw.print("while(");
		expression.genC(pw, false);
		pw.println(") {");
		pw.add();	
		//if(s != null)
		//System.out.println("statement do while: " + s);
		s.genC(pw);
		pw.sub();
		pw.println("}");

	}

	public Expr getExpression() {
		return expression;
	}
	public void setExpression(Expr expression) {
		this.expression = expression;
	}

	public Statement getS() {
		return s;
	}

	public void setS(Statement s) {
		this.s = s;
	}

	public WhileStatement(Expr expression, Statement s) {
		this.expression = expression;
		this.s = s;
	}

	private Expr expression;
	private Statement s;
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("while(");
		expression.genKra(pw, putParenthesis);
		pw.println(") {");
		pw.add();
		s.genKra(pw, putParenthesis);
		pw.sub();
		pw.println("}");
		
	}
}
