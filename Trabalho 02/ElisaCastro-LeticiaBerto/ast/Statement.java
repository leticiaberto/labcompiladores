/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

abstract public class Statement {

	abstract public void genC(PW pw);

	abstract public void genKra(PW pw, boolean putParenthesis);

}
