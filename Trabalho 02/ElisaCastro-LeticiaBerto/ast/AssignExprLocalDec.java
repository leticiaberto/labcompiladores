/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;

public class AssignExprLocalDec extends Expr {

	@Override
	public void genC(PW pw, boolean putParenthesis) {
		//System.out.println("to na area");
		if(vl != null) {
			pw.add();
			vl.genC(pw,putParenthesis);
			pw.sub();
		} else {
			//System.out.println("esquerdo");
			if(esquerdo != null) {
				//pw.add();
				//System.out.println("esquerdo");
				esquerdo.genC(pw, putParenthesis);
				if(direito != null) {
					//System.out.println("direito");
					pw.print(" = ");
					direito.genC(pw, putParenthesis);
				}
				//pw.sub();
				pw.printlnIdent(";");
			}
			
		}
		//pw.println();
		//direito.genC(pw,false);
		//esquerdo.genC(pw,false);
	}

	@Override
	public Type getType() {
		// TODO Auto-generated method stub
		return null;
	}

	private VariableList vl;
	private Expr direito;
	private Expr esquerdo;

	public AssignExprLocalDec(Expr esquerdo, Expr direito) {
		this.direito = direito;
		this.esquerdo = esquerdo;
		this.vl = null;
	}

	public AssignExprLocalDec(VariableList vl) {
		this.vl = vl;
		this.direito = null;
		this.esquerdo = null;
	}
	
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		if(vl != null) {
			vl.genKra(pw, putParenthesis);
		} else {
			if(esquerdo != null) {
				esquerdo.genKra(pw, putParenthesis);
				if(direito != null) {
					pw.print(" = ");
					direito.genKra(pw, putParenthesis);
				}
			}
			
		}
	}

	@Override
	public Type getMethType() {
		// TODO Auto-generated method stub
		return null;
	}

}
