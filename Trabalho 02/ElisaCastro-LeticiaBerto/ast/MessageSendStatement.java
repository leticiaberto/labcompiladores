/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class MessageSendStatement extends MessageSend {


   private KraClass classe;
   private Variable v;

public MessageSendStatement(Variable v, KraClass currentClass) {
		// TODO Auto-generated constructor stub
	   this.v = v;
	   this.classe = currentClass;
	}

public void genC( PW pw, boolean putParenthesis ) {
      //pw.printIdent("");
	pw.add();
    pw.printIdent("this->_" + classe.getName() + "_" + v.getName());//coloquei agora
    pw.sub();
    // messageSend.genC(pw);
//      /pw.println(";");
   }

   private MessageSend  messageSend;

@Override
public void genKra(PW pw, boolean putParenthesis) {
	// TODO Auto-generated method stub
	
}

@Override
public Type getType() {
	// TODO Auto-generated method stub
	return v.getType();
}

@Override
public Type getMethType() {
	
	return null;
}

}


