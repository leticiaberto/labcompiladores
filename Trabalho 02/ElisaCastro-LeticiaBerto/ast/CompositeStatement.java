/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;


public class CompositeStatement extends Statement {

	@Override
	public void genC(PW pw) {
		for(Statement s : statementList) {
			s.genC(pw);
		}
	}


	public ArrayList<Statement> getStatementList() {
		return statementList;
	}


	public void setStatementList(ArrayList<Statement> statementList) {
		this.statementList = statementList;
	}


	public CompositeStatement(ArrayList<Statement> statementList) {
		this.statementList = statementList;
	}


	private ArrayList<Statement> statementList;


	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		for(Statement s : statementList) {
			s.genKra(pw, putParenthesis);
		}
		
	}
}
