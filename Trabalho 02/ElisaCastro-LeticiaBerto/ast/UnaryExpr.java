/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/
package ast;

import lexer.*;

public class UnaryExpr extends Expr {

	public UnaryExpr(Expr expr, Symbol op) {
		this.expr = expr;
		this.op = op;
	}

	@Override
	public void genC(PW pw, boolean putParenthesis) {
		switch (op) {
		case PLUS:
			pw.print("+");
			break;
		case MINUS:
			pw.print("-");
			break;
		case NOT:
			pw.print("!");
			break;
		default:
			pw.print(" internal error at UnaryExpr::genC");

		}
		expr.genC(pw, false);
	}

	@Override
	public Type getType() {
		return expr.getType();
	}
	
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print(op.toString());
		if(expr != null) {
			pw.print(expr.toString() + ";");
		}
		
	}

	private Expr	expr;
	private Symbol	op;
	@Override
	public Type getMethType() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
