/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

public class TypeVoid extends Type {

    public TypeVoid() {
        super("void");
    }

   public String getCname() {
      return "void";
   }

}
