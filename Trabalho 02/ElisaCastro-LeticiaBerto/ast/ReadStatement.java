/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;

public class ReadStatement extends Statement{

	@Override
	public void genC(PW pw) {
		int i;
		pw.println();
		pw.println("{");
		pw.add();
		for(i = 0; i < varList.size(); i++) {
			pw.printlnIdent("char __" + i + "[512];");
		}
		
		//pw.add();
		for(i = 0; i < varList.size(); i++) {
			pw.printlnIdent("gets(__" + i + ");");
		}
		i = 0;
		for(Variable v : varList) {
			
			if(v.getType() == Type.intType) {
				pw.printIdent("sscanf(__" + i + ", \"%d\", &");
				pw.printlnIdent("_" + v.getName() + ");");
			} else if(v.getType() == Type.stringType) {
				pw.printlnIdent("_" + v.getName() + " = malloc(strlen(__s) + 1);");
				pw.printlnIdent("strcpy(_" + v.getName() + ", __s);");
			}
			i++;
			
			/*else {//if(v.getType() instanceof KraClass) {
				System.out.println("readzinho");
				//pw.print("sscanf(__s, \"%d\", &");
				//pw.print("this->_<nomeClasse>" + v.getName());
			}*/
		}
		pw.sub();
		pw.println("}");
		pw.println();
	}

	public ArrayList<Variable> getVarList() {
		return varList;
	}

	public void setVarList(ArrayList<Variable> varList) {
		this.varList = varList;
	}

	public ReadStatement(ArrayList<Variable> varList) {
		this.varList = varList;
	}

	private ArrayList<Variable> varList;

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("read(");
		for(Variable v : varList) {
			v.genKra(pw, putParenthesis);
		}
		
	}

}
