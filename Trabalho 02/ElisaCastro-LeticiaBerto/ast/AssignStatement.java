/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class AssignStatement extends Statement {

	@Override
	public void genC(PW pw) {
		//System.out.println("assign");
		//System.out.println("assign inst " + expression);
		expression.genC(pw, false);
	}

	public AssignStatement(Expr expression) {
		this.expression = expression;
	}

	private Expr expression;

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		expression.genKra(pw, putParenthesis);
	}
}
