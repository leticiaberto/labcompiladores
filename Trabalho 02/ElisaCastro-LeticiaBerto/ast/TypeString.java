/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

public class TypeString extends Type {

    public TypeString() {
        super("String");
    }

   public String getCname() {
      return "char *";
   }

}
