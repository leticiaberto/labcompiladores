/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/


package ast;

import java.util.Iterator;


public class WriteStatement extends Statement{

	@Override
	public void genC(PW pw) {
		pw.add();
		int flagInt = 0;
		int sizeList = expressionList.getSize();
		if(expressionList.elements() != null) {
			//System.out.println("size: " + expressionList.getSize());
			Iterator<Expr> it = expressionList.elements();
			Iterator<Expr> itAux = expressionList.elements();
			int flagstring = 0;
			int flagMessage = 0;
			int flagBool = 0;
			Expr iter = null;
			while(itAux.hasNext()) {
				iter = itAux.next();
				if(iter instanceof MessageSend) {
					flagMessage = 1;
					break;
				}
				else if(iter.getType().equals(Type.intType)) {
					flagInt = 1;
					break;
				}
				else if (iter.getType().equals(Type.stringType)) {
					flagstring = 1;
					break;
				}
				else if(iter.getType().equals(Type.booleanType)) {
					flagBool = 1;
					break;
				}
				
			}
			
			if(flagInt == 1) {
				pw.printIdent("printf(\"");
				for(int i = 0; i < sizeList; i++) {
					pw.print("%d ");
				}
				pw.print("\", ");
				int count = sizeList-1;
				while(it.hasNext()) {
					it.next().genC(pw, false);
					if(count >= 1) {
						pw.print(", ");
					}
					count--;
					
					
					//pw.print(it.toString());
					//it.next();
				}
				
				//pw.println(");");
				
			} else if(flagstring == 1) {
				pw.printIdent("puts(");
				while(it.hasNext()) {
					it.next().genC(pw, false);
				}
				
			} /*else if(flagBool == 1) {
				it.next().genC(pw, false);
			}*/
			else if(flagMessage == 1){
				Iterator<Expr> itE = null;
				
				//System.out.print("oooooooooooooooooooolllaaa");
				pw.printIdent("printf (\"%d\", ((");
			
				if(expressionList != null) {
					
					itE = expressionList.elements();
					while(itE.hasNext()) {
						//System.out.print("oooooooooooooooooooolllaaa: " + itE.next());
						itE.next().genC(pw, false);
					}
				}
				//iter.genC(pw, false);
			
			}
			pw.println(");");		
			
		}
		pw.sub();
		
	}

	public ExprList getExpressionList() {
		return expressionList;
	}

	public void setExpressionList(ExprList expressionList) {
		this.expressionList = expressionList;
	}

	public WriteStatement(ExprList expressionList) {
		this.expressionList = expressionList;
	}

	private ExprList expressionList;

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		if(expressionList.elements() != null) {
			//System.out.println("size: " + expressionList.getSize());
			Iterator<Expr> it = expressionList.elements();
			pw.printIdent("write(");
			while(it.hasNext()) {
				//System.out.println("it: " + it.next().genKra(pw, putParenthesis));
				it.next().genKra(pw, putParenthesis);
				//pw.print(it.toString());
				//it.next();
			}
			
			pw.println(");");	
		}
	}
}
