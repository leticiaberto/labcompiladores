/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class IfStatement extends Statement{

	@Override
	public void genC(PW pw) {
		pw.add();
		pw.printIdent("if(");
		pw.sub();
		if(expression != null) {
			expression.genC(pw, false);
		}
		pw.println(") {");
		pw.add();
		if(sIf != null) { 
			sIf.genC(pw);
		}	
		//pw.add();
		pw.printIdent("}");
		pw.sub();
		if(sElse != null) {
			pw.println(" else {");
			if(sElse != null) {
				sElse.genC(pw);
			}			
			pw.println("}");
		} else {
			pw.println();
		}
		
		
		
		
		/*pw.print("if(");
		expression.genC(pw, false);
		pw.println(") {");
		pw.add();
		sIf.genC(pw);
		pw.sub();
		pw.print("}");
		if(sElse != null) {
			pw.println(" else {");
			pw.add();
			sElse.genC(pw);
			pw.sub();
			pw.println("}");
		}*/

	}

	public Statement getsElse() {
		return sElse;
	}
	public void setsElse(Statement sElse) {
		this.sElse = sElse;
	}


	public Statement getsIf() {
		return sIf;
	}


	public void setsIf(Statement sIf) {
		this.sIf = sIf;
	}


	public Expr getExpression() {
		return expression;
	}


	public void setExpression(Expr expression) {
		this.expression = expression;
	}


	public IfStatement(Expr expression, Statement sIf, Statement sElse) {
		this.expression = expression;
		this.sIf = sIf;
		this.sElse = sElse;
	}


	private Expr expression;
	private Statement sIf;
	private Statement sElse;
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("if(");
		expression.genKra(pw, putParenthesis);
		pw.println(") {");
		pw.add();
		sIf.genKra(pw, putParenthesis);
		pw.sub();
		pw.print("}");
		if(sElse != null) {
			pw.println(" else {");
			pw.add();
			sElse.genKra(pw, putParenthesis);
			pw.sub();
			pw.println("}");
		}
		
	}
}
