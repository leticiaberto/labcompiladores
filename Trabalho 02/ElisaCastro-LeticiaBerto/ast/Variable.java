/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

public class Variable {

    public Variable( String name, Type type ) {
        this.name = name;
        this.type = type;
    }

    public String getName() { return name; }

    public Type getType() {
        return type;
    }
    
    public void genKra(PW pw, boolean putParenthesis) {
		pw.printIdent(type.getName());
		pw.print(" " + name);
		pw.print(";");
	}
    
    public void genC(PW pw) {
    	if(type instanceof KraClass) {
    		pw.printIdent("_" + type.getCname() + " *_" + name);
    	} else {
    		//if(type.getName().compareTo("String") == 0) {
    		if(type == Type.stringType) {	
    			pw.printIdent("char *_" + name);
    		} else
    			pw.printIdent(type.getName() + " _" + name);
    	}
    		
    }
    
 // this-> ????
 	// usado quando a variavel eh uma expressao ou esta do lado direito de '='
 	// colocar em variable tambem
 	public String getCnameExpr() {
 		return "this->" + this.getCname();
 	}
 	
 // usado para declaracao da variavel quando ela nao eh uma expressao
 	public String getCname() {
 		
 		//return classe.getCname() + "_" + name;
 		return "_" + name;
 	}

 	
    private String name;
    private Type type;
}
