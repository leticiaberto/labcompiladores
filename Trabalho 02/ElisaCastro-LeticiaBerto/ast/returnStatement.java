/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class returnStatement extends Statement {



	@Override
	public void genC(PW pw) {
		pw.add();
		pw.printIdent("return ");
		pw.sub();
		if(expression != null) {
			//if(expression instanceof VariableExpr)
				//System.out.print("tipo ");
			expression.genC(pw,false);
			//pw.print("");
		}
		pw.print(";");
		//talvez precise do ; aqui
		pw.println();
	}

	public Expr getExpression() {
		return expression;
	}

	public void setExpression(Expr expression) {
		this.expression = expression;
	}

	public returnStatement(Expr expression) {
		this.expression = expression;
	}

	private Expr expression;

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		//pw.print("return ");
		
	}

}
