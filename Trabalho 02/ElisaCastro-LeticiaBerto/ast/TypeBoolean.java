/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

public class TypeBoolean extends Type {

   public TypeBoolean() { super("boolean"); }

   @Override
   public String getCname() {
      return "int";
   }

}
