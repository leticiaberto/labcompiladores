/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;


public class VariableExpr extends Expr {

    public VariableExpr( Variable v, boolean this_ ) {
        this.v = v;
        this.this_ = this_;
    }

    
    @Override
    public void genC( PW pw, boolean putParenthesis ) {
        //pw.print( v.getName() );
        //System.out.println("eu quero");
    	
        if(v != null) {
    		if(this_) {
    			pw.add();
    			pw.printIdent("this->");
    			pw.sub();
    		}
    		//System.out.println("quero dormir: " + v.getName());
    		//v.genC(pw);
    		pw.print("_" + v.getName());
    		
    		//pw.print(v.getCnameExpr());
    		
    		
    		
    		//System.out.println(v.getClass() + " <- blah classe");
    		//v.genC(pw);
    		//pw.print(v.genC(pw, false));
    	}
    }

    public Type getType() {
        return v.getType();
    }
    
    @Override
    public void genKra(PW pw, boolean putParenthesis) {
    	if(v != null) {
    		if(this_) {
    			pw.printIdent("this.");
    		}
    		pw.print(v.getName());
    	}
    	
    }
    
    public String getNome() {
    	return v.getName();
    }

    private Variable v;
    private boolean this_;
    
	@Override
	public Type getMethType() {
		return null;
	}
}
