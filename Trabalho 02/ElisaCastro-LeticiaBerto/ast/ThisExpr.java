package ast;

public class ThisExpr extends Expr {

	private KraClass currentClass;

	public ThisExpr(KraClass currentClass) {
		this.currentClass = currentClass;
	}
	@Override
	public void genC(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
		pw.print("this->");
	}

	@Override
	public Type getType() {
		// TODO Auto-generated method stub
		return currentClass;
	}

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("this.");
		
	}
	@Override
	public Type getMethType() {
		// TODO Auto-generated method stub
		return null;
	}

}
