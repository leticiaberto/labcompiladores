/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;
import java.util.Iterator;

import lexer.Symbol;

/*
 * Krakatoa Class
 */
public class KraClass extends Type {
	
	
	// fazer uma lista soh para guardar 
	
	//a.put()
	// ver o tipo do a: A. Procurar no A o metodo put. O metodo vai ter o numero 1
	
	// ArrayList<String> methodNameList; // o indice ja vai estar na ordem. Achou retono o indice
	//lista de metodos publicos!!!! Ja tem o indice

	   public KraClass( String name ) {
	      super(name);
	      this.name = name;
	      this.instanceVariableList = new InstanceVariableList();
	      this.publicMethodList = new ArrayList<MethodDec>();
	  	  this.indexList = new ArrayList<String>();
	   }

	   public String getCname() {
	      return "class_" + getName();
	      //return getName();
	   }

	   public KraClass getSuperclass() {
		   return superclass;
	   }

	   public void setSuperclass(KraClass superclass) {
		   this.superclass = superclass;
		   //indexList = superclass.getIndexList();
		  // for(String h : indexList) {
			   //System.out.println("aet: " + h);	
		  // }
		  if (superclass.getIndexList() != null){
			  // System.out.println("eai");
			for(MethodDec m: superclass.getIndexList()){
				//System.out.println(m.getName());
				publicMethodList.add(m);
			}
			//for(MethodDec m: publicMethodList)
				//System.out.println(m.getName());
		   }
		   //this.publicMethodList = superclass.getIndexList();
		  //this.publicMethodList = superclass.getIndexList();
	   }

	   public ArrayList<MethodDec> getIndexList(){
		   return publicMethodList;
	   }
	   public InstanceVariable searchInstanceVariable(String name){
		   InstanceVariable instV = this.instanceVariableList.getInstanceVariable(name);
		   return instV;
		   /*for (InstanceVariable v : this.instanceVariableList.getInstanceVariableList()){
			   if (name.equals(v.getName()))
				   return v;
		   }
		   return null;*/
	   }

	   //elisa
	    public void addInstVar(InstanceVariable instV) {
	    	this.instanceVariableList.addElement(instV);
		}

	   public void addMethod(MethodDec currentMethod) {
		   int flagIndex = 0;
			// adicionar na lista de metodos
		  // publicMethodList.add(currentMethod);
		   //indexList.add(currentMethod.getName());
		/*   for(String h : indexList) {
			   System.out.println("agrrrret: " + h);	
		  }*/
		   /*System.out.println("AddMethod");
		   for(MethodDec m: publicMethodList)
				System.out.println(m.getName());
		   */
		   if(publicMethodList != null){
			   for(MethodDec mth : publicMethodList){
				  if (mth.getName().equals(currentMethod.getName())) {
					  int posicao = positionMethod(mth.getName());
					   //System.out.println("Retorno índice: " + superclass.positionMethod(mth.getName()));
					   publicMethodList.remove(posicao);
			
					   //System.out.println("current method class " + currentMethod.getClasse().getName());
					   publicMethodList.add(posicao, currentMethod);
					   //currentMethod.setClasse();
					   /*MethodDec novoM = publicMethodList.get(posicao);
					   System.out.println("ja tem: " + novoM.getName() + " nome classe: " + novoM.getClasse().getName());
					   */
					   //System.out.println("tamanho depois de adicionar: " + publicMethodList.size());
					   
					   /*System.out.println("Vetor index: ");
					   for(MethodDec h : publicMethodList) {
						   System.out.println(h.getName() + " " + h.getClasse().getName());	
					   }*/
					   flagIndex = 1;
					   break;
				   }
			   }
		   }
		   if(flagIndex == 0)  {
			   
			   publicMethodList.add(currentMethod);
		   }//System.out.println("if add");
		   //indexList.add(currentMethod.getName());
		 //  System.out.println("Quualquer coisa ");
		
	   }

	   public int positionMethod(String nameM) {
		  // System.out.println("nnomito: " + nameM);
		//	System.out.println("no indexdjfghhv n : " + indexList.size());
		//	System.out.println("index de nome : " + indexList.indexOf(nameM));
		//	for(String n : indexList) {
			//	System.out.println("no index: " + n);
		//	}
		   int contador = 0;
		   for(MethodDec m : publicMethodList) {
			   
			   //System.out.println("nome: " + m.getName());
			   if(m.getName().equals(nameM)) {
				   return contador;
				   //return m.getName().indexOf(nameM);
			   }
			   contador++;
				   //  System.out.println("metoos:     " + m.getName().indexOf(nameM));
		   }
			//System.out.print("pos kra: " + nameM + " " + publicMethodList.getName().indexOf(nameM));
			//return publicMethodList.indexOf(nameM);
		   //return indexList.indexOf(nameM);
		   return -1;
		}	   
	   
	   public MethodDec getMethod(String name) {
		   for(MethodDec m : publicMethodList) {
			   if(m.getName().equals(name)) {
				   return m;
			   }
		   }
		   return null;
		}
	   
	   public void genKra(PW pw, boolean putParenthesis) {
		   //System.out.println("");
		    pw.print("class " + name);
			if(superclass != null) {
				pw.print(" extends " + superclass.getName());				
			}
			pw.println(" {");
			//pw.println();
			pw.add();
			
			if(instanceVariableList != null) {
				instanceVariableList.genKra(pw, putParenthesis);
			}
			pw.println();
			if(publicMethodList != null) {
				//System.out.printl	this.indexList = new ArrayList<String>();n("na lista de metodos");
				//pw.add();
				for(MethodDec met : publicMethodList) {
					//System.out.println("nome: " + met.getName());
					
					met.genKra(pw, putParenthesis);
				}
			}
			//pw.sub();
			pw.println("}");
		}
	   
	   public void genC(PW pw) {
		   pw.println("typedef");
		   pw.add();
		   pw.printlnIdent("struct _St_" + name + " {");
		   pw.add();
		   pw.printlnIdent("Func *vt;");
		   if(superclass != null) {
			   //System.out.println("classe atual: " + name);
			   //while para percorrer todas as superclasses
			   ArrayList<InstanceVariable> instanceVariableListSuper;
			   instanceVariableListSuper = superclass.instanceVariableList.getInstanceVariableList();
			   //instanceVariableListSuper.getClasse();
			  // System.out.println("size: " + instanceVariableListSuper.size());
			   for(InstanceVariable vl : instanceVariableListSuper) {
				 //  System.out.println("superclasse " +vl.getClasse().getName());
				  // System.out.println("var " + vl.getCname());
				   pw.printlnIdent(vl.getType().getName() + " " +  vl.getCname() + ";");
				   //vl.genC(pw);
			   }
		   }
		   if(instanceVariableList != null) {
			   instanceVariableList.genC(pw,name);
		   }
		   pw.sub();
		   pw.printlnIdent("} _class_" + name + ";");
		   pw.sub();
		   pw.println();
		   
		   pw.println("_class_" + name + " *new_" + name + "(void);");
		   pw.println();
		   
		   if(publicMethodList != null) {
				//System.out.println("na lista de metodos");
				//pw.add();
				for(MethodDec met : publicMethodList) {
					//System.out.println("nome: " + met.getName());
					
					met.genC(pw, name);
				}
			}
		   
		   pw.println();
		   
		   pw.printlnIdent("Func VTclass_" + name + "[] = {");
		   int count = publicMethodList.size();
		   pw.add();
		   for(MethodDec met : publicMethodList) {
				//System.out.println("nome: " + met.getName());
			  if(met.getQualifier() != Symbol.PRIVATE) {
				  pw.printIdent("(void" + " (*)()) _" + met.getClasse().getName() + "_" + met.getName());
				   count--;
				   if(count != 0) {
					   pw.println(",");
				   }
			  }
			  
		   }
		   pw.sub();
		   pw.println();
		   pw.println("};");
		   pw.println();
		   
		   pw.println("_class_" + name + " *new_" + name + "() {");
		   pw.add();
		   pw.printlnIdent("_class_" + name + " *t;");
		   pw.println();
		   pw.printlnIdent("if((t = malloc(sizeof(_class_" + name + "))) != NULL)");
		   pw.add();
		   pw.printlnIdent("t->vt = VTclass_" + name + ";");
		   pw.sub();
		   pw.println();
		   pw.printlnIdent("return t;");
		   pw.println("}");
		   pw.sub();
		   pw.println();
	   }

	   private String name;
	   private KraClass superclass;
	   private InstanceVariableList instanceVariableList;
	   // private MethodList publicMethodList, privateMethodList;
	   private ArrayList<MethodDec> publicMethodList;
	   private ArrayList<String> indexList;
	   // métodos públicos get e set para obter e iniciar as variáveis acima,
	   // entre outros métodos
	



}
