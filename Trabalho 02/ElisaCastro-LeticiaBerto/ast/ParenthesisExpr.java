/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;


public class ParenthesisExpr extends Expr {

    public ParenthesisExpr( Expr expr ) {
        this.expr = expr;
    }

    public void genC( PW pw, boolean putParenthesis ) {
        pw.print("(");
        expr.genC(pw, false);
        pw.printIdent(")");
    }

    public Type getType() {
        return expr.getType();
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
		
	}

    private Expr expr;

	@Override
	public Type getMethType() {
		// TODO Auto-generated method stub
		return null;
	}
}
