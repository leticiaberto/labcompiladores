/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;


import lexer.Symbol;

public class MethodDec {
	//qualifier, tipo de retorno, parametros, corpo, nome
	public MethodDec(Symbol qualifier, String name, Type type, ArrayList<Statement> statement, ParamList p) {
		this.qualifier = qualifier;
		this.name = name;
		this.type = type;
		this.statement = statement;
		this.methodList = new ArrayList<MethodDec>();
		this.p = p;
		this.classe = null;
	
	}
	
	public void setClasse(KraClass classe) {
		this.classe = classe;
	}
	
	public KraClass getClasse() {
		return this.classe;
	}

	public Symbol getQualifier() {
		return qualifier;
	}

	public void setQualifier(Symbol qualifier) {
		this.qualifier = qualifier;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public ArrayList<Statement> getStatement() {
		return statement;
	}

	public void setStatement(ArrayList<Statement> statement) {
		this.statement = statement;
	}

	public ArrayList<MethodDec> getMethodList() {
		return methodList;
	}

	public void setMethodList(ArrayList<MethodDec> methodList) {
		this.methodList = methodList;
	}

	public MethodDec getMethod(String name) {
		for(MethodDec m : methodList) {
			   if(m.getName().equals(name)) {
				   return m;
			   }
		}
		return null;
	}

	public void addElement(MethodDec currentMethod) {
		methodList.add(currentMethod);
	}
	
	

	public int getSizeParamList() {
		if ( p == null ) 
			return 0;
		return p.getSize();
	}

	public ParamList getParamList() {
		return p;
	}
	
	public void genKra(PW pw, boolean putParenthesis) {
		if(methodList != null) {
			pw.printIdent(qualifier.toString() + " " + type.getName() + " " + name + "(");
			if(p != null) {
				p.genKra(pw, putParenthesis);
			}
			pw.print(") {");
			if(statement != null) {
				pw.add();
				pw.println();
				if(statement != null) {
					for(Statement s : statement) {
						s.genKra(pw, putParenthesis);
					}
				}
				pw.sub();
			}
			pw.printlnIdent("}");
		}		
	}
	
	public void genC(PW pw, String nameClass) {
		pw.printIdent(type.getName() + " _" + nameClass + "_" + name + "(_class_" + nameClass + " *this");
		if(p.getSize() > 0) { 
			pw.print(", ");
			p.genC(pw);
		}
		pw.println(") {");
		if(statement != null) {
			for(Statement s : statement) {
				s.genC(pw);
			}
		}
		pw.add();
		pw.printlnIdent("printf(\"\\n\");");
		pw.sub();
		pw.println("}");
		
		pw.println();
	}

	private Symbol qualifier;
	private String name;
	private Type type;
	private ArrayList<Statement> statement;
	private ArrayList<MethodDec> methodList;
	private ParamList p;
	private KraClass classe;
	
	
	
}
