/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.*;

public class InstanceVariableList {

	private KraClass classe;


	public void setClass(KraClass classe){
		this.classe = classe;
	}
	
	public KraClass getClasse(){
		return classe;
	}
	
    public InstanceVariableList() {
       instanceVariableList = new ArrayList<InstanceVariable>();
    }

    public void addElement(InstanceVariable instanceVariable) {
       instanceVariableList.add( instanceVariable );
    }

    public Iterator<InstanceVariable> elements() {
    	return this.instanceVariableList.iterator();
    }

    public int getSize() {
        return instanceVariableList.size();
    }


	public ArrayList<InstanceVariable> getInstanceVariableList() {
		return instanceVariableList;
	}

	// retorna um InstanceVariable caso ele exista no InstanceVariableList
	public InstanceVariable getInstanceVariable(String name) {
		if(instanceVariableList.size() > 0) {
			for(InstanceVariable instV : instanceVariableList) {
				if(instV.getName().equals(name)) {
					return instV;
				}
			}
		}
		return null;
	}

	public void setInstanceVariableList(
			ArrayList<InstanceVariable> instanceVariableList) {
		this.instanceVariableList = instanceVariableList;
	}

	
	public void genKra(PW pw, boolean putParenthesis) {
		if(instanceVariableList != null) {
			for(InstanceVariable instV : instanceVariableList) {
				instV.genKra(pw, putParenthesis);
			}
		}
		
	}
	
	
	public void genC(PW pw, String nameClass) {
		if(instanceVariableList != null) {
			for(InstanceVariable instV : instanceVariableList) {
				//pw.printlnIdent(instV.getType().getName() + " _" + nameClass + "_" + instV.getName() + ";");
				//pw.printlnIdent(instV.getType().getName() + "--");
				//System.out.println("jjjjjjjjjjjjjjjjj");
				instV.genC(pw, false);
				pw.println(";");
			}
		}
		
	}
	
	
	 private ArrayList<InstanceVariable> instanceVariableList;

	

}
