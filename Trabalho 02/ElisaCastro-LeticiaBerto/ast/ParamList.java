/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

import java.util.*;

public class ParamList {

    public ParamList() {
       paramList = new ArrayList<Variable>();
    }

    public void addElement(Variable v) {
       paramList.add(v);
    }

    public Iterator<Variable> elements() {
        return paramList.iterator();
    }

    public int getSize() {
        return paramList.size();
    }
    
    public void genKra(PW pw, boolean putParenthesis) {
		if(paramList != null) {
			for(Variable var : paramList) {
				//var.genKra(pw, putParenthesis);
				pw.print(var.getType().getName() + " " + var.getName());
				// aqui eh lista de parametros dentro da declaracao do metodo
				// tem que ser separado por ",", porem em Variable coloca ;
			}
		}
	}
    
    public void genC(PW pw) {
    	int count;
    	if(paramList != null) {
    		count = paramList.size();
			for(Variable var : paramList) {
				var.genC(pw);
				count--;
				if(count != 0)
					pw.print(", ");
				
				//pw.print(var.getType().getName() + " " + var.getName());
				// aqui eh lista de parametros dentro da declaracao do metodo
				// tem que ser separado por ",", porem em Variable coloca ;
			}
		}
    }

    private ArrayList<Variable> paramList;

	

}
