/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;



public class doWhileStatement extends Statement{

	public doWhileStatement(Statement compositeList, Expr expression) {
		this.setCompositeList(compositeList);
		this.setExpression(expression);
	}

	public Statement getCompositeList() {
		return compositeList;
	}
	public void setCompositeList(Statement compositeList2) {
		this.compositeList = compositeList2;
	}

	public Expr getExpression() {
		return expression;
	}

	public void setExpression(Expr expression) {
		this.expression = expression;
	}


	@Override
	public void genC(PW pw) {
		pw.print("do {");
		pw.add();
		if(compositeList != null) {
			compositeList.genC(pw);
		}
		
		pw.sub();
		pw.print("} while(");
		if(expression != null) {
			expression.genC(pw, false);
		}
		
		pw.print(");");
	}

	private Statement compositeList;
	private Expr expression;
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("do {");
		pw.add();
		if(compositeList != null) {
			compositeList.genKra(pw, putParenthesis);
		}
		
		pw.sub();
		pw.print("} while(");
		if(expression != null) {
			expression.genKra(pw, putParenthesis);
		}
		
		pw.print(");");
		
	}
}
