/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;


public class LiteralString extends Expr {

    public LiteralString( String literalString ) {
        this.literalString = literalString;
    }

    public void genC( PW pw, boolean putParenthesis ) {
        pw.print("\"" + literalString + "\"");
    }

    public Type getType() {
        return Type.stringType;
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
    	 pw.print(literalString);
	}

    private String literalString;

	@Override
	public Type getMethType() {
		// TODO Auto-generated method stub
		return null;
	}
}
