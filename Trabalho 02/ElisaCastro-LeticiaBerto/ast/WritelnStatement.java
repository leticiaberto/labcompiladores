/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.Iterator;


public class WritelnStatement extends Statement{

	@Override
	public void genC(PW pw) {
		pw.add();
		if(expressionList != null) {
			Iterator<Expr> it = expressionList.elements();
			pw.printIdent("puts(");
			pw.sub();
			while(it.hasNext()) {
				it.next().genC(pw, false);
			}
			pw.printlnIdent(");");
		}
		//pw.sub();

	}

	public ExprList getExpressionList() {
		return expressionList;
	}

	public void setExpressionList(ExprList expressionList) {
		this.expressionList = expressionList;
	}


	public WritelnStatement(ExprList expressionList) {
		this.expressionList = expressionList;
	}

	private ExprList expressionList;

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
		
	}
}
