/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.ArrayList;

public class VariableList  {

	public VariableList(ArrayList<Variable> v) {
		this.v = v;
	}

	public void genKra(PW pw, boolean putParenthesis) {
		for(Variable var : v) {
			var.genKra(pw, putParenthesis);
			//; em Variable.java
			//dar enter aqui?
			pw.println();
		}
	}
	public void genC(PW pw, boolean putParenthesis) {
		for(Variable var : v) {
			var.genC(pw);
			//; em Variable.java
			//dar enter aqui?
			//pw.println();
		}
	}
	private ArrayList<Variable> v;
}
