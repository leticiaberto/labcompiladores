/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class Parameter extends Variable {

    public Parameter( String name, Type type ) {
        super(name, type);
    }

}
