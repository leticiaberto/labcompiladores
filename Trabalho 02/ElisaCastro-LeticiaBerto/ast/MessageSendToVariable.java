/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.Iterator;



public class MessageSendToVariable extends MessageSend {

    public MessageSendToVariable(Variable v, MethodDec auxMeth,
			ExprList exprList, KraClass classKra) {
		this.v = v;
		this.auxMeth = auxMeth;
		this.exprList = exprList;
		this.classKra = classKra;
	}

	public MessageSendToVariable(KraClass classKra) {
		this.classKra = classKra;
	}

	public Type getType() {
        return auxMeth.getType();
    }
	
	public Type getMethType() {
		return auxMeth.getType();
	}

    public void genC( PW pw, boolean putParenthesis ) {
    	//pw.print("this->");
    	Iterator<Expr> cont;
    	pw.print("((" + auxMeth.getType().getName() + "(*)(_" + v.getType().getCname() + " *");
    	if (exprList != null){
			cont = exprList.elements();
			while(cont.hasNext()){
				pw.print(", " + cont.next().getType().getName());
			}
		}
    	KraClass a = (KraClass) v.getType();
    	pw.print(")) _" + v.getName() + "->vt[" + a.positionMethod(auxMeth.getName()) + "])(_" + v.getName());
    	//pw.print(")) _" + v.getName() + "->vt[<indice>])(_" + v.getName());
    	if (exprList != null){
			cont = exprList.elements();
			while(cont.hasNext()){
				pw.print(", ");
				cont.next().genC(pw, putParenthesis);
			}
		}
    	pw.print(")");	// ; está vindo de parametros
    	
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
    	Iterator<Expr> cont;
		pw.print(v.getName() + "." + auxMeth.getName() + "(");
		if (exprList != null){
			cont = exprList.elements();
			while(cont.hasNext()){
				cont.next().genKra(pw, putParenthesis);
			}
		}
		pw.println(")");
		
	}

    private Variable v;
    private MethodDec auxMeth;
	private ExprList exprList;
	private KraClass classKra;

}
