/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

import java.util.*;


public class ExprList {

    public ExprList() {
        exprList = new ArrayList<Expr>();
    }

    public void addElement( Expr expr ) {
        exprList.add(expr);
    }

    public int getSize() {
    	return exprList.size();
    }

    // para conseguir percorrer a lista
    public Iterator<Expr> elements() {
		return exprList.iterator();
	}

    public void genC( PW pw ) {

        int size = exprList.size();
        for ( Expr e : exprList ) {
        	e.genC(pw, false);
            if ( --size > 0 )
                pw.print(", ");
        }
    }

    private ArrayList<Expr> exprList;
    
    public void genKra(PW pw, boolean putParenthesis) {
    	if(exprList != null) {
    		for(Expr e : exprList) {
    			e.genKra(pw, putParenthesis);
    		}
    	}
    	
    }

}
