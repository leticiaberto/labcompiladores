/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class BreakStatement extends Statement {

	@Override
	public void genC(PW pw) {
		pw.print("break");
	}

	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.print("break");
	}


}
