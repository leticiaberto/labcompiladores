/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

import java.util.Iterator;



public class MessageSendToSelf extends MessageSend {

    public MessageSendToSelf(KraClass currentClass, MethodDec meth,
			ExprList exprList) {
    	this.currentClass = currentClass;
    	this.meth = meth;
    	this.exprList = exprList;
    	this.v = null;
	}

	public MessageSendToSelf(KraClass currentClass, Variable v,
			MethodDec meth, ExprList exprList) {
		this.currentClass = currentClass;
    	this.meth = meth;
    	this.exprList = exprList;
    	this.v = v;
	}

	public MessageSendToSelf(KraClass currentClass, Variable v) {
		this.currentClass = currentClass;
    	this.v = v;
	}

	public MessageSendToSelf(KraClass currentClass) {
		this.currentClass = currentClass;
	}

	public Type getType() {
		if(v != null)
			return v.getType();
		return null;
    }

    public void genC( PW pw, boolean putParenthesis ) {
    	
    	
    	if(currentClass.getMethod(meth.getName()) == null) {
    		pw.print(meth.getType().getCname() + "(*)(_");
    		KraClass superAux = currentClass.getSuperclass();
    		while(superAux.getMethod(meth.getName()) == null)
    			superAux = superAux.getSuperclass();
    		
    		pw.print(superAux.getCname() + " *)) this->vt[" + superAux.positionMethod(meth.getName()) + "])((_" + superAux.getCname() + " *)this)");
    	} else {
    		pw.print("_" + currentClass.getName() + "_" + meth.getName() + "(this");
    		if(exprList != null) {
    			//int count = exprList.getSize();
    			pw.print(", ");
    			exprList.genC(pw);
    		}
    		pw.print(")");
    	}
    	
    	
    	
    	/*pw.print("this.");
    	if(v != null) {
    		pw.print(v.getName() + ".");
    	}
    	pw.print(meth.getName());
    	pw.print("(");
    	if (exprList != null){
    		exprList.genC(pw);
    	}
    	pw.print(");");*/
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
    	pw.print("this.");
    	if(v != null) {
    		pw.print(v.getName() + ".");
    	}
    	pw.print(meth.getName());
    	pw.print("(");
    	if (exprList != null){
    		exprList.genKra(pw, putParenthesis);
    	}
    	pw.print(");");
	}

    private KraClass currentClass;
    private MethodDec meth;
    private ExprList exprList;
    private Variable v;
	@Override
	public Type getMethType() {
		return meth.getType();
	}
}
