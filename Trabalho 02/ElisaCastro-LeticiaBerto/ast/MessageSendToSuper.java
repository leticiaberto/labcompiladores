/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

import java.util.Iterator;


public class MessageSendToSuper extends MessageSend {

    public MessageSendToSuper(MethodDec m, ExprList exprList, KraClass currentC) {
		// TODO Auto-generated constructor stub
    	this.m = m;
    	this.exprList = exprList;
    	this.currentC = currentC;
	}

	public Type getType() {
        return null;
    }

    public void genC( PW pw, boolean putParenthesis ) {
    	pw.print("_" + currentC.getName() + "_" + m.getName() +"((_" + currentC.getCname() + " *)this");
    	if (exprList != null){
    		pw.print(",");
    		Iterator<Expr> n = exprList.elements();
    		int count = exprList.getSize();
    		while(n.hasNext()) {
				//var.genC(pw);
    			n.next().genC(pw, false);
				count--;
				if(count != 0)
					pw.print(", ");
			}
    	}
    	pw.print(")");
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
		// TODO Auto-generated method stub
		
	}

    private MethodDec m;
    private ExprList exprList;
    private KraClass currentC;
	@Override
	public Type getMethType() {
		return m.getType();
	}
}
