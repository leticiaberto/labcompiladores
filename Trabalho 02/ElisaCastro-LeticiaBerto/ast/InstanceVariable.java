/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class InstanceVariable extends Variable {

    private KraClass classe;
    private String name;
    private Type tipo;
    
	public InstanceVariable( String name, Type type, KraClass classe ) {
        super(name, type);
        this.classe = classe;
        this.name = name;
        this.tipo = type;
    }

	public void genC(PW pw, boolean putParenthesis){
		//System.out.print("classe imts " + classe.getName());
		
		pw.printIdent(tipo.getName() + " _"+ classe.getName()+"_"+ name);
	}
	
	public KraClass getClasse() {
		return classe;
	}
	
	// this-> ????
	// usado quando a variavel eh uma expressao ou esta do lado direito de '='
	// colocar em variable tambem
	public String getCnameExpr() {
		return "this." + this.getCname();
	}
	
	// usado para declaracao da variavel quando ela nao eh uma expressao
	public String getCname() {
		return classe.getName() + "_" + name;
	}
}
