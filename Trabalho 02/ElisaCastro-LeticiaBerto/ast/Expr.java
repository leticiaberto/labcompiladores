/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


abstract public class Expr {
    abstract public void genC( PW pw, boolean putParenthesis );
      // new method: the type of the expression
    abstract public Type getType();
    abstract public void genKra(PW pw, boolean putParenthesis);
    abstract public Type getMethType();
}
