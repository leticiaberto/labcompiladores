/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

public class TypeInt extends Type {

    public TypeInt() {
        super("int");
    }

   public String getCname() {
      return "int";
   }

}
