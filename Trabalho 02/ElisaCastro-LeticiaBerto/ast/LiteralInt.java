/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;


public class LiteralInt extends Expr {

    public LiteralInt( int value ) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    public void genC( PW pw, boolean putParenthesis ) {
        pw.print("" + value);
    }

    public Type getType() {
        return Type.intType;
    }
    
    @Override
	public void genKra(PW pw, boolean putParenthesis) {
    	pw.print("" + value);
	}

    private int value;

	@Override
	public Type getMethType() {
		return null;
	}
}
