/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;


public class NewObject extends Expr{

	private KraClass theClass;

	public NewObject(KraClass aClass) {
		this.theClass = aClass;
	}

	@Override
	public void genC(PW pw, boolean putParenthesis) {
		pw.print("new_" + theClass.getName() + "()");
	}

	@Override
	public Type getType() {
		return theClass;
	}
	
	@Override
	public void genKra(PW pw, boolean putParenthesis) {
		pw.println("new " + theClass.getCname() + "();");
		
	}

	@Override
	public Type getMethType() {
		return null;
	}

}
