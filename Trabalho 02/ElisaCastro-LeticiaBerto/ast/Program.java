/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

import java.util.*;

import comp.CompilationError;

public class Program {

	public Program(ArrayList<KraClass> classList, ArrayList<MetaobjectCall> metaobjectCallList,
			       ArrayList<CompilationError> compilationErrorList) {
		this.classList = classList;
		this.metaobjectCallList = metaobjectCallList;
		this.compilationErrorList = compilationErrorList;
	}


	public void genKra(PW pw) {
		if(classList != null) {
			//System.out.println("tamanho: " + classList.size());
			for(KraClass classK : classList) {
				classK.genKra(pw, true);
			}
		}
	}

	public void genC(PW pw) {
		pw.println("#include <malloc.h>");
		pw.println("#include <stdlib.h>");
		pw.println("#include <stdio.h>");
		pw.println();
		pw.println("typedef int boolean;");
		pw.println("#define true 1");
		pw.println("#define false 0");
		pw.println();
		pw.println("typedef");
		pw.add();
		pw.printlnIdent("void (*Func)();");
		pw.sub();
		pw.println();
		if(classList != null) {
			//System.out.println("tamanho: " + classList.size());
			for(KraClass classK : classList) {
				classK.genC(pw);
			}
		}
		pw.println();
		pw.println("int main() {");
		pw.println();
		pw.add();
		pw.printlnIdent("_class_Program *program;");
		pw.println();
		pw.printlnIdent("program = new_Program();");
		pw.printlnIdent("((void(*)(_class_Program*))program->vt[0])(program);");
		pw.println();
		pw.printlnIdent("return 0;");
		pw.println();
		pw.sub();
		pw.print("}");
		
	}

	public ArrayList<KraClass> getClassList() {
		return classList;
	}


	public ArrayList<MetaobjectCall> getMetaobjectCallList() {
		return metaobjectCallList;
	}


	public boolean hasCompilationErrors() {
		return compilationErrorList != null && compilationErrorList.size() > 0 ;
	}

	public ArrayList<CompilationError> getCompilationErrorList() {
		return compilationErrorList;
	}


	private ArrayList<KraClass> classList;
	private ArrayList<MetaobjectCall> metaobjectCallList;

	ArrayList<CompilationError> compilationErrorList;


}
