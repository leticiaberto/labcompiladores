/*
    Elisa Castro    RA: 587303
    Leticia Berto   RA: 587354
*/

package ast;

import java.util.*;


public class LocalVariableList {

    public LocalVariableList() {
       localList = new ArrayList<Variable>();
    }

    public void addElement(Variable v) {
       localList.add(v);
    }

    public Iterator<Variable> elements() {
        return localList.iterator();
    }

    public int getSize() {
        return localList.size();
    }

    public void genKra(PW pw, boolean putParenthesis){
    	if (localList != null){
    		for (Variable v : localList)
    			v.genKra(pw, putParenthesis);
    	}
    }
    private ArrayList<Variable> localList;

}
