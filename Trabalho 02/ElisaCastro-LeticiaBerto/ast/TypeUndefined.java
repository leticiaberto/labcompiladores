/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/

package ast;

public class TypeUndefined extends Type {
    // variables that are not declared have this type

   public TypeUndefined() { super("undefined"); }

   public String getCname() {
      return "int";
   }

}
