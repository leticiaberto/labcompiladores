/*
	Elisa Castro	RA: 587303
	Leticia Berto	RA: 587354
*/
package comp;

import ast.*;
import lexer.*;

import java.io.*;
import java.util.*;

public class Compiler {

	private KraClass currentClass;
	// compile must receive an input with an character less than
	// p_input.lenght
	public Program compile(char[] input, PrintWriter outError) {

		ArrayList<CompilationError> compilationErrorList = new ArrayList<>();
		signalError = new ErrorSignaller(outError, compilationErrorList);
		symbolTable = new SymbolTable();
		lexer = new Lexer(input, signalError);
		signalError.setLexer(lexer);

		Program program = null;
		lexer.nextToken();
		program = program(compilationErrorList);
		return program;
	}

	private Program program(ArrayList<CompilationError> compilationErrorList) {
		// Program ::= KraClass { KraClass }
		ArrayList<MetaobjectCall> metaobjectCallList = new ArrayList<>();
		ArrayList<KraClass> kraClassList = new ArrayList<>();
		//Program program = new Program(kraClassList, metaobjectCallList, compilationErrorList);
		try {
			while ( lexer.token == Symbol.MOCall ) {
				metaobjectCallList.add(metaobjectCall());
			}
			kraClassList.add(classDec());
			//System.out.println("entrei uma");
			while ( lexer.token == Symbol.CLASS ) {
				//System.out.println("entrei antes: " + lexer.token);
				kraClassList.add(classDec());
				//System.out.println("entrei aqui: " + lexer.token);
			}
		//	System.out.println("depois while: " + lexer.token);
				
			if(symbolTable.getInGlobal("Program") == null) {
				signalError.showError("Source code without a class 'Program'");
			}
			if ( lexer.token != Symbol.EOF ) {
				signalError.showError("End of file expected");
			}
		}
		catch( RuntimeException e) {
			// if there was an exception, there is a compilation signalError
			//e.printStackTrace();
		}
		//Program program = new Program(kraClassList, metaobjectCallList, compilationErrorList);
		return new Program(kraClassList, metaobjectCallList, compilationErrorList);
	}

	/**  parses a metaobject call as <code>{@literal @}ce(...)</code> in <br>
     * <code>
     * @ce(5, "'class' expected") <br>
     * clas Program <br>
     *     public void run() { } <br>
     * end <br>
     * </code>lexer.token)
     *

	 */
	@SuppressWarnings("incomplete-switch")
	private MetaobjectCall metaobjectCall() {
		String name = lexer.getMetaobjectName();
		lexer.nextToken();
		ArrayList<Object> metaobjectParamList = new ArrayList<>();
		if ( lexer.token == Symbol.LEFTPAR ) {
			// metaobject call with parameters
			lexer.nextToken();
			while ( lexer.token == Symbol.LITERALINT || lexer.token == Symbol.LITERALSTRING ||
					lexer.token == Symbol.IDENT ) {
				switch ( lexer.token ) {
				case LITERALINT:
					metaobjectParamList.add(lexer.getNumberValue());
					break;
				case LITERALSTRING:
					metaobjectParamList.add(lexer.getLiteralStringValue());
					break;
				case IDENT:
					metaobjectParamList.add(lexer.getStringValue());
				}
				lexer.nextToken();
				if ( lexer.token == Symbol.COMMA )
					lexer.nextToken();
				else
					break;
			}
			if ( lexer.token != Symbol.RIGHTPAR )
				signalError.showError("')' expected after metaobject call with parameters");
			else
				lexer.nextToken();
		}
		if ( name.equals("nce") ) {
			if ( metaobjectParamList.size() != 0 )
				signalError.showError("Metaobject 'nce' does not take parameters");
		}
		else if ( name.equals("ce") ) {
			if ( metaobjectParamList.size() != 3 && metaobjectParamList.size() != 4 )
				signalError.showError("Metaobject 'ce' take three or four parameters");
			if ( !( metaobjectParamList.get(0) instanceof Integer)  )
				signalError.showError("The first parameter of metaobject 'ce' should be an integer number");
			if ( !( metaobjectParamList.get(1) instanceof String) ||  !( metaobjectParamList.get(2) instanceof String) )
				signalError.showError("The second and third parameters of metaobject 'ce' should be literal strings");
			if ( metaobjectParamList.size() >= 4 && !( metaobjectParamList.get(3) instanceof String) )
				signalError.showError("The fourth parameter of metaobject 'ce' should be a literal string");

		}

		return new MetaobjectCall(name, metaobjectParamList);
	}

	private KraClass classDec() {
		// Note que os métodos desta classe não correspondem exatamente às
		// regras
		// da gramática. Este método classDec, por exemplo, implementa
		// a produção KraClass (veja abaixo) e partes de outras produções.

		/*
		 * KraClass ::= ``class'' Id [ ``extends'' Id ] "{" MemberList "}"
		 * MemberList ::= { Qualifier Member }
		 * Member ::= InstVarDec | MethodDec
		 * InstVarDec ::= Type IdList ";"
		 * MethodDec ::= Qualifier Type Id "("[ FormalParamDec ] ")" "{" StatementList "}"
		 * Qualifier ::= [ "static" ]  ( "private" | "public" )
		 */
		if ( lexer.token != Symbol.CLASS ) signalError.showError("'class' expected");
		lexer.nextToken();
		if ( lexer.token != Symbol.IDENT )
			signalError.show(ErrorSignaller.ident_expected);
		String className = lexer.getStringValue();
		if (symbolTable.getInGlobal(className) != null)
			signalError.showError("Class '" + className + "' already exists");
		currentClass = new KraClass(className);
		symbolTable.putInGlobal(className, currentClass);

		lexer.nextToken();
		if ( lexer.token == Symbol.EXTENDS ) {
			lexer.nextToken();
			if ( lexer.token != Symbol.IDENT )
				signalError.show(ErrorSignaller.ident_expected);
			String superclassName = lexer.getStringValue();
			if(superclassName.equals(className))
				signalError.showError("Class '" + className + "' is inheriting from itself");
			KraClass superClass = this.symbolTable.getInGlobal(superclassName);
			if (superClass != null)
				currentClass.setSuperclass(superClass);
			else
				signalError.showError("Superclasse não existe");
			lexer.nextToken();
		}
		if ( lexer.token != Symbol.LEFTCURBRACKET )
			signalError.showError("{ expected", true);
		lexer.nextToken();

		while (lexer.token == Symbol.PRIVATE || lexer.token == Symbol.PUBLIC) {

			Symbol qualifier;
			switch (lexer.token) {
			case PRIVATE:
				lexer.nextToken();
				qualifier = Symbol.PRIVATE;
				break;
			case PUBLIC:
				lexer.nextToken();
				qualifier = Symbol.PUBLIC;
				break;
			default:
				signalError.showError("private, or public expected");
				qualifier = Symbol.PUBLIC;
			}
			Type t = type();
			if ( lexer.token != Symbol.IDENT )
				signalError.showError("Identifier expected");
			String name = lexer.getStringValue();
			lexer.nextToken();
			if ( lexer.token == Symbol.LEFTPAR ) {
				methodDec(t, name, qualifier);
			}

			else if ( qualifier != Symbol.PRIVATE )
				signalError.showError("Attempt to declare a public instance variable");
			else {
				instanceVarDec(t, name);
			}

		}

		// verificar se a classe eh Program, se for, verificar se existe o metdodo run
		if(currentClass.getName().equals("Program")) {
			// olhar em uma lista de metodo? Ou fazer o searchMethod
			MethodDec meth = currentClass.getMethod("run");
			if(meth == null) {
				signalError.showError("Method 'run' was not found in class 'Program'");
			}
		}

		if ( lexer.token != Symbol.RIGHTCURBRACKET )
			signalError.showError("public/private or \"}\" expected");
		lexer.nextToken();
		
		return currentClass;

	}

	private void instanceVarDec(Type type, String name) {
		// InstVarDec ::= [ "static" ] "private" Type IdList ";"
		InstanceVariableList listInstV = new InstanceVariableList();
		//listInstV.setClass(currentClass);
		
		if (currentClass.searchInstanceVariable(name) == null) {
			InstanceVariable instV = new InstanceVariable(name, type, currentClass);
			currentClass.addInstVar(instV);
			listInstV.addElement(instV);

		} else {
			signalError.showError("Variable '" + name + "' is being redeclared");
		}
		while (lexer.token == Symbol.COMMA) {
			lexer.nextToken();
			if ( lexer.token != Symbol.IDENT )
				signalError.showError("Identifier expected");
			name = lexer.getStringValue();
			//if(symbolTable.getInLocal(name) == null) {
			if(currentClass.searchInstanceVariable(name) == null) {
				InstanceVariable instV = new InstanceVariable(name, type, currentClass);
				listInstV.addElement(instV);
				currentClass.addInstVar(instV);
				//Variable v = new Variable(name, type);
				//symbolTable.putInLocal(name, v);
			} else {
				signalError.showError("Variable '" + name + "' is being redeclared");
			}
			lexer.nextToken();
		}

		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();
	}

	private void methodDec(Type type, String name, Symbol qualifier) {
		/*
		 * MethodDec ::= Qualifier Return Id "("[ FormalParamDec ] ")" "{"
		 *                StatementList "}"
		 */

		//metodo tem o mesmo nome de uma variavel de instancia
		//Variable v = symbolTable.getInLocal(name);
		ParamList p = null;

		InstanceVariable instV = currentClass.searchInstanceVariable(name);
		if(instV != null) {
			signalError.showError("Method '" + name + "' has name equal to an instance variable");
		}

		// run da classe Program tem que retornar void
		if(type != Type.voidType && name.equals("run") && currentClass.getName().equals("Program")) {
			signalError.showError("Method 'run' of class 'Program' with a return value type different from 'void'");
		}

		lexer.nextToken();

		if(name.equals("run") && qualifier == Symbol.PRIVATE && currentClass.getName().equals("Program")) {
			signalError.showError("Method 'run' of class 'Program' cannot be private");
		}
		if ( lexer.token != Symbol.RIGHTPAR ) {
			// pode ter um metodo run, mas que n'ao esta na classe Program
			if((name.equals("run") && !currentClass.getName().equals("Program")) || !name.equals("run")) {
				p = formalParamDec();
			} else {
				if(currentClass.getName().equals("Program"))
					signalError.showError("Method 'run' of class 'Program' cannot take parameters");
			}
		}
		else {
			p = new ParamList();
		}

		if ( lexer.token != Symbol.RIGHTPAR )
			signalError.showError(") expected");

		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTCURBRACKET )
			signalError.showError("{ expected");

		//lexer.nextToken(); -- isso foi pra baixo da verificação, porque daí o erro aparece na linha certa
		// no lugar do null tem que ser o corpo do metodo
		currentMethod = new MethodDec(qualifier, name, type, null, p);
		//ver se esta sendo redeclarado
		// se tiver mais de um metodo com mesmo nome, qual retorna?
		MethodDec met = currentClass.getMethod(name);
		
		
		
	
		
		if(met != null) { //&& (classMethod == currentClass)) {
			KraClass classMethod = met.getClasse();
			//System.out.println("Classe atual: " + currentClass.getName());
			//System.out.println("Classe classMethod: " + classMethod.getName());
			if(classMethod.getName().compareTo(currentClass.getName()) == 0) {
				//System.out.println("entrei ondenao devia");
				signalError.showError("Method '" + name + "' is being redeclared");
			}
			/*if(met.getQualifier() != qualifier || met.getType() != type) {
				//busca so nos metodos dessa classe
				// privado da classe corrente e publicos da super
				// DENTRO DE KRACLASS
				// searchPrivatePublicThisClassMethods
				// searchPrivateThisClassPublicSuperclassesMethods
				// searchPublicMethods
				signalError.showError("Method '" + name + "' is being redefined");
			}*/
			
		} else {
			//System.out.println("tenho superclasse");
			KraClass metClass = currentClass.getSuperclass();
			if(metClass != null) {
				MethodDec methClass = metClass.getMethod(name);
				if(methClass != null) {
					if(methClass.getQualifier() != qualifier || methClass.getType() != type ||
						
						//signalError.showError("Method '" + name + "' is being redefined2");
					//}
							methClass.getSizeParamList() != p.getSize())
						signalError.showError("Method '" + name + "' is being redefined");
					if(methClass.getQualifier() == qualifier && methClass.getType() == type
						&& methClass.getSizeParamList() == p.getSize()) {
						if(p.getSize() > 0) {
							Iterator<Variable> itP = p.elements();
							Iterator<Variable> it = methClass.getParamList().elements();
							int countParam = 0;
							Variable v;
							Variable vP;
							while(it.hasNext() && itP.hasNext()) {
								v = it.next();
								vP = itP.next();
								if(v.getType().equals(vP.getType())) {
									countParam++;
								}
								if(!v.getType().equals(vP.getType())) {
									signalError.showError("Method '" + name + "' is being redefined3");
								}
							}
							if(countParam != p.getSize())
								signalError.showError("metodo sendo redefinido com tipos de parametros diferentes");
						}
					}
				}
			}
		}
		lexer.nextToken();
			/*if(met.getQualifier() == qualifier && met.getType() == type) {
				if(met.getSizeParamList() == p.getSize()) {
					if(met.getSizeParamList() > 0) {
						// percorrer a lista de parametros e verificar se sao compativeis
						Iterator<Variable> it = met.getParamList().elements();
						int countParam = 0;
						while(it.hasNext()) {
							if(it.next().getType().equals(type)) {
								countParam++;
							}
							if(!it.next().getType().equals(type)) {
								signalError.showError("Method '" + name + "' is being redefined");
							}
						}
						if(countParam == p.getSize())
							signalError.showError("Method '" + name + "' is being redeclared");
					}
				} else {
					signalError.showError("Method '" + name + "' is being redefined");
				}
			} else if(met.getQualifier() != qualifier || met.getType() != type) {
				if(met.getSizeParamList() == p.getSize() && p.getSize() > 0) {
					//verificar se os tipos sao diferentes
					Iterator<Variable> itMeth = met.getParamList().elements();
					Iterator<Variable> itP = p.elements();
					while(itMeth.hasNext()) {
						// instanceof
						if(!itMeth.next().getType().equals(itP.next().getType())) {
							signalError.showError("Method '" + name + "' is being redefined");
						}
					}
				}
				signalError.showError("Method '" + name + "' is being redefined");
			}*/
		//System.out.println("compiler " + currentMethod.getName() + "class " + currentClass.getName());
		currentMethod.setClasse(currentClass);
		currentClass.addMethod(currentMethod);
		

		ArrayList<Statement> listState = statementList();
		currentMethod.setStatement(listState);
		int flagReturn = 0;
		if(type != Type.voidType) {
			for(Statement s : listState) {
				if(s instanceof returnStatement) {
					flagReturn = 1;
				} else {
					// verificar se dentro desse statement tem return: posso ter um s = if, e dentro desse if tem return
					if(s instanceof IfStatement) {
						flagReturn = 1;
					}
				}
			}
			if(flagReturn == 0) {
				signalError.showError("Missing 'return' statement in method '" + name + "'");
			}
		}

		if ( lexer.token != Symbol.RIGHTCURBRACKET )
			signalError.showError("} expected");
		symbolTable.removeLocalIdent();
		lexer.nextToken();

	}

	private Expr localDec() {
		// LocalDec ::= Type IdList ";"
		ArrayList<Variable> localVL = new ArrayList<Variable>();
		Variable v = null;
		Type type = type();
		if ( lexer.token != Symbol.IDENT ) signalError.showError("Identifier expected");
		if(symbolTable.getInLocal(lexer.getStringValue()) == null) {
			v = new Variable(lexer.getStringValue(), type);
			symbolTable.putInLocal(lexer.getStringValue(), v);
			localVL.add(v);
		} else {
				signalError.showError("Variable '" + lexer.getStringValue() + "' is being redeclared");
		}

		//} else {
		//	signalError.showError("Variable " + lexer.getStringValue() + " is being redeclared");
		//}
		lexer.nextToken();
		while (lexer.token == Symbol.COMMA) {
			lexer.nextToken();
			//System.out.println(lexer.getStringValue() + "<- ");
			if ( lexer.token != Symbol.IDENT )
				signalError.showError("Identifier expected");
			//v = new Variable(lexer.getStringValue(), type);
			//symbolTable.putInLocal(lexer.getStringValue(), v);
			//if(symbolTable.getInGlobal(lexer.getStringValue()) == null) {
				if(symbolTable.getInLocal(lexer.getStringValue()) == null) {
					v = new Variable(lexer.getStringValue(), type);
					symbolTable.putInLocal(lexer.getStringValue(), v);
					localVL.add(v);
				} else {
					signalError.showError("Variable '" + lexer.getStringValue() + "' is being redeclared");
				}
			//} else {
			//	signalError.showError("Variable " + lexer.getStringValue() + " is being redeclared");
			//}
			lexer.nextToken();
		}

		// caso seja um =, quer dizer que estou declarando e atribuindo
		// isso deve ser tratado no assignExprLocalDec()
		//Expr direito = null;
		if ( lexer.token == Symbol.ASSIGN ) {
			lexer.nextToken();
			// direito = expr();
			//if(direito == null) {
			//	signalError.showError("Expression expected in the right-hand side of assignment");
			//}
		} else {

			if(lexer.token != Symbol.SEMICOLON) {
				signalError.show(ErrorSignaller.semicolon_expected);
				//signalError.showError("Missing ';'");
			}
		}
		lexer.nextToken();

		VariableList vl = new VariableList(localVL);
		return new AssignExprLocalDec(vl);
	}

	private ParamList formalParamDec() {
		// FormalParamDec ::= ParamDec { "," ParamDec }
		ParamList p = new ParamList();

		p.addElement(paramDec());
		while (lexer.token == Symbol.COMMA) {
			lexer.nextToken();
			p.addElement(paramDec());
		}
		return p;
	}

	private Variable paramDec() {
		// ParamDec ::= Type Id

		Type t = type();
		if ( lexer.token != Symbol.IDENT )
			signalError.showError("Identifier expected");

		String name = lexer.getStringValue();
		if(symbolTable.getInLocal(name) != null) {
			signalError.showError("Parameter '" + name + "' already exists");
		}
		Variable var = new Variable(name, t);
		symbolTable.putInLocal(name, var);
		lexer.nextToken();

		return var;
	}

	private Type type() {
		// Type ::= BasicType | Id
		Type result;

		switch (lexer.token) {
		case VOID:
			result = Type.voidType;
			break;
		case INT:
			result = Type.intType;
			break;
		case BOOLEAN:
			result = Type.booleanType;
			break;
		case STRING:
			result = Type.stringType;
			break;
		case IDENT:
			// # corrija: faça uma busca na TS para buscar a classe
			// IDENT deve ser uma classe.
			KraClass v = symbolTable.getInGlobal(lexer.getStringValue());
			if (v == null)
				signalError.showError("Class '" + lexer.getStringValue() + "' not declared");
			result = v;
			break;
		default:
			signalError.showError("Type expected");
			result = Type.undefinedType;
		}
		lexer.nextToken();
		return result;
	}

	private CompositeStatement compositeStatement() {

		ArrayList<Statement> listStatement;

		lexer.nextToken();
		listStatement = statementList();
		if ( lexer.token != Symbol.RIGHTCURBRACKET )
			signalError.showError("} expected");
		else
			lexer.nextToken();

		return new CompositeStatement(listStatement);

	}

	private ArrayList<Statement> statementList() {
		// CompStatement ::= "{" { Statement } "}"
		Symbol tk;
		ArrayList<Statement> statementL = new ArrayList<Statement>();

		// statements always begin with an identifier, if, read, write, ...
		while ((tk = lexer.token) != Symbol.RIGHTCURBRACKET
				&& tk != Symbol.ELSE)
			statementL.add(statement());

		return statementL;
	}

	private Statement statement() {
		/*
		 * Statement ::= Assignment ``;'' | IfStat |WhileStat | MessageSend
		 *                ``;'' | ReturnStat ``;'' | ReadStat ``;'' | WriteStat ``;'' |
		 *               ``break'' ``;'' | ``;'' | CompStatement | LocalDec
		 */

		Statement statementReturn = null;
		switch (lexer.token) {
		case THIS:
		case IDENT:
		case SUPER:
		case INT:
		case BOOLEAN:
		case STRING:
			//Expr assELD = assignExprLocalDec();
			statementReturn = new AssignStatement(assignExprLocalDec());
			break;
		case ASSERT:
			//Statement assSt = assertStatement();
			statementReturn = assertStatement();
			break;
		case RETURN:
			statementReturn = returnStatement();
			break;
		case READ:
			statementReturn = readStatement();
			break;
		case WRITE:
			statementReturn = writeStatement();
			break;
		case WRITELN:
			statementReturn = writelnStatement();
			break;
		case IF:
			statementReturn = ifStatement();
			break;
		case BREAK:
			statementReturn = breakStatement();
			break;
		case WHILE:
			flagWhile++;
			statementReturn = whileStatement();
			break;
		case SEMICOLON:
			statementReturn = nullStatement();
			break;
		case LEFTCURBRACKET:
			statementReturn = compositeStatement();
			break;
		case DO:
			statementReturn = doWhileStatement();
			break;
		default:
			signalError.showError("Statement expected");
		}

		return statementReturn;
	}

	private Statement assertStatement() {
		lexer.nextToken();
		int lineNumber = lexer.getLineNumber();
		Expr e = expr();
		if ( e.getType() != Type.booleanType )
			signalError.showError("boolean expression expected");
		if ( lexer.token != Symbol.COMMA ) {
			this.signalError.showError("',' expected after the expression of the 'assert' statement");
		}
		lexer.nextToken();
		if ( lexer.token != Symbol.LITERALSTRING ) {
			this.signalError.showError("A literal string expected after the ',' of the 'assert' statement");
		}
		String message = lexer.getLiteralStringValue();
		lexer.nextToken();
		if ( lexer.token == Symbol.SEMICOLON )
			lexer.nextToken();

		return new StatementAssert(e, lineNumber, message);
	}

	/*
	 * retorne true se 'name' é uma classe declarada anteriormente. É necessário
	 * fazer uma busca na tabela de símbolos para isto.
	 */
	private boolean isType(String name) {
		return this.symbolTable.getInGlobal(name) != null;
	}

	/*
	 * AssignExprLocalDec ::= Expression [ ``$=$'' Expression ] | LocalDec
	 */
	private Expr assignExprLocalDec() {

		Expr var;
		if ( lexer.token == Symbol.INT || lexer.token == Symbol.BOOLEAN
				|| lexer.token == Symbol.STRING ||
				// token é uma classe declarada textualmente antes desta
				// instrução
				(lexer.token == Symbol.IDENT && isType(lexer.getStringValue()) && lexer.temIgualouPonto() != Symbol.ASSIGN && lexer.temIgualouPonto() != Symbol.DOT)) {
			/*
			 * uma declaração de variável. 'lexer.token' é o tipo da variável
			 *
			 * AssignExprLocalDec ::= Expression [ ``$=$'' Expression ] | LocalDec
			 * LocalDec ::= Type IdList ``;''
			 */
			var = localDec();
			return new AssignExprLocalDec(var, null);

		}
		else {
			/*
			 * AssignExprLocalDec ::= Expression [ ``$=$'' Expression ]
			 */
			//expr();
			Expr esquerdo = expr();
			/*if(lexer.token == Symbol.SEMICOLON) {
				Variable v = symbolTable.getInLocal(esquerdo.toString());
				if(v == null) {
					signalError.showError("Statement expected2");
				}

			}*/
			Type tipoEsquerdo = esquerdo.getType();
			//System.out.println("Tipo esquerdo: " + tipoEsquerdo);
			
			Expr direito = null;

			if ( lexer.token == Symbol.ASSIGN ) {
				lexer.nextToken();
				direito = expr();
				if(direito == null) {
					signalError.showError("Expression expected in the right-hand side of assignment");
				}


				//se o tipo for uma classe, temos que checar a compatibilidade entre as expressões
				if(tipoEsquerdo instanceof KraClass){
					//a segunda não pode ser null
					if(direito.getType() != null){

						//ver se a classe é subtipo
						//Type typeSuper =  vSuperMeth.getType();
						Type typeDir = direito.getType();
						boolean flagSubClass = false;
						if(tipoEsquerdo.getName().equals(typeDir.getName())) {
							flagSubClass = true;
						} else {
							KraClass classKra = symbolTable.getInGlobal(typeDir.getName());
							if(classKra != null) {
								classKra = classKra.getSuperclass();
								while(classKra != null) {
									if(classKra.getName().equals(tipoEsquerdo.getName())) {
										flagSubClass = true;
									}
									classKra = classKra.getSuperclass();
								}
							}
						}
						if(!flagSubClass)
							signalError.showError("Type error: value of the right-hand side is not subtype of the variable of the left-hand side.");


						//checo os tipos
						/*boolean check = checkClassType(left.getType(), right.getType());
						//se voltar false, não são compatíveis
						if(!check){
							signalError.show("The type of the right expression can't be converted to the first expression's type");
						}*/

					} else {
						//não faço nada, objetos podem receber null
					}

				} else {
					//se os tipos são simples, só comparo mesmo
					//primeiro faço o caso do null
					//if(direito.getType() == null) {
						//System.out.println("hey thereeee: ");
					//}
					
					if((tipoEsquerdo == Type.intType || tipoEsquerdo == Type.stringType ||
							tipoEsquerdo == Type.voidType || tipoEsquerdo == Type.booleanType)
							&& (!(direito instanceof MessageSendToSelf) && direito.getType() == null))
						signalError.showError("'null' cannot be assigned to a variable of a basic type");
					//senão, os dois são tipos definidos
					if(!tipoEsquerdo.getName().equals(direito.getType().getName())){
						signalError.showError("tipos diferentes");
					}
				}
			}
			//tratando o caso dos tipos nulos
			if(esquerdo != null && direito == null){
				//System.out.println("diferente de null: " + tipoEsquerdo + " " + esquerdo);
				//esquerda não pode ser void, se for inteiro ele ta querendo usar expr como instrução, também não pode
				if(tipoEsquerdo != Type.voidType){
					if(tipoEsquerdo == Type.intType){
						signalError.showError("cannot use expression as instruction");
					}
				}
				if(!(tipoEsquerdo instanceof KraClass)) {
					//System.out.println("entrei ta: " + esquerdo.getMethType() );
					if(esquerdo.getMethType() != Type.voidType) {
						signalError.showError("errinho de classe");
					}
					
				}
			}
			if ( lexer.token != Symbol.SEMICOLON )
				signalError.show(ErrorSignaller.semicolon_expected);
				//signalError.showError("Missing ';'");
			else
				lexer.nextToken();
			if(direito != null){
				return new AssignExprLocalDec(esquerdo, direito);
			}else{
				return new AssignExprLocalDec(esquerdo, null);
			}



				//expr();
				//nao pode ter uma classe igual a um tipo basico
				/*if((tipoEsquerdo instanceof KraClass) && (direito.getType() == Type.intType || direito.getType() == Type.stringType
						|| direito.getType() == Type.booleanType||
						direito.getType() == Type.voidType))
				if((tipoEsquerdo != Type.intType && tipoEsquerdo != Type.stringType &&
						tipoEsquerdo != Type.booleanType && tipoEsquerdo != Type.voidType) &&
						(direito.getType() == Type.intType || direito.getType() == Type.stringType
						|| direito.getType() == Type.booleanType||
						direito.getType() == Type.voidType)) {
					signalError.showError("Type error: the type of the expression of the right-hand side is a basic type and the type of the variable of the left-hand side is a class");
				}
				//nao pode ter um tipo basico igual uma classe (contrario da de cima)
				if((tipoEsquerdo == Type.intType || tipoEsquerdo == Type.stringType
						|| tipoEsquerdo == Type.booleanType|| tipoEsquerdo == Type.voidType) &&
						(direito.getType() != Type.intType && direito.getType() != Type.stringType &&
								direito.getType() != Type.booleanType && direito.getType() != Type.voidType)) {
					signalError.showError("Type error: type of the left-hand side of the assignment is a basic type and the type of the right-hand side is a class");
				}
				// nao pode ter tipo basico igual a null
				if((tipoEsquerdo == Type.intType || tipoEsquerdo == Type.stringType ||
						tipoEsquerdo == Type.booleanType || tipoEsquerdo == Type.voidType) && direito.getType() == null) {
					signalError.showError("Type error: 'null' cannot be assigned to a variable of a basic type");
				}
				// int nao pode ser assigned a boolean
				if(tipoEsquerdo == Type.booleanType && direito.getType() == Type.intType) {
					signalError.showError("'int' cannot be assigned to 'boolean'");
				}
				if(direito.getType() != tipoEsquerdo) {
					signalError.showError("Type error: value of the right-hand side is not subtype of the variable of the left-hand side.");
				}
				// direito tem que ser do mesmo tipo ou subtipo do lado esquerdo
				if(direito.getType() instanceof KraClass) {

					//ver se a classe é subtipo
					//Type typeSuper =  vSuperMeth.getType();
					Type typeDir = direito.getType();
					boolean flagSubClass = false;
					if(tipoEsquerdo.getName().equals(typeDir.getName())) {
						flagSubClass = true;
					} else {
						KraClass classKra = symbolTable.getInGlobal(typeDir.getName());
						if(classKra != null) {
							classKra = classKra.getSuperclass();
							while(classKra != null) {
								if(classKra.getName().equals(tipoEsquerdo.getName())) {
									flagSubClass = true;
								}
								classKra = classKra.getSuperclass();
							}
						}
					}
					if(!flagSubClass)
						signalError.showError("Type error: value of the right-hand side is not subtype of the variable of the left-hand side.");

						//signalError.showError("classes não são subtipos");

					//signalError.showError("Type error: value of the right-hand side is not subtype of the variable of the left-hand side.");
				}
				if((tipoEsquerdo instanceof KraClass) && (direito.getType() instanceof KraClass)) {
					if(esquerdo.getClass() != direito.getClass()) {
						if(direito.getClass().getSuperclass() != esquerdo.getClass()) {
							signalError.showError("Type error: value of the right-hand side is not subtype of the variable of the left-hand side.");
						}
					}
				}

				if ( lexer.token != Symbol.SEMICOLON )
					signalError.showError("';' expected", true);
				else
					lexer.nextToken();
			}
			return new AssignExprLocalDec(esquerdo, direito);*/
		}
	}

	private ExprList realParameters() {
		ExprList anExprList = null;
		
		//System.out.println(lexer.getStringValue());
		//lexer.nextToken();
		//System.out.println(lexer.getStringValue());
		if ( lexer.token != Symbol.LEFTPAR ) 
			signalError.showError("( expected");
		
		lexer.nextToken();
		if ( startExpr(lexer.token) ) anExprList = exprList();
		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();
		return anExprList;
	}

	private WhileStatement whileStatement() {

		Expr expression;

		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
		lexer.nextToken();
		expression = expr();
		if(expression.getType() != Type.booleanType) {
			signalError.showError("non-boolean expression in 'while' command");
		}
		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();
		Statement s = statement();

		return new WhileStatement(expression, s);
	}

	private IfStatement ifStatement() {

		Statement sElse = null;

		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
		lexer.nextToken();
		Expr expression = expr();
		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();
		Statement sIf = statement();
		if ( lexer.token == Symbol.ELSE ) {
			lexer.nextToken();
			sElse = statement();
		}

		return new IfStatement(expression, sIf, sElse);
	}

	private returnStatement returnStatement() {

		KraClass auxKraClass = null;

		lexer.nextToken();
		Expr expression = expr();
		if(expression != null && currentMethod.getType() == Type.voidType)
			signalError.showError("Illegal 'return' statement. Method returns 'void'");
		
		/*if(expression.getType()) {
			
		}*/
		
		if(expression.getType() != currentMethod.getType()) {
			if(expression.getType() instanceof KraClass) {
				auxKraClass = symbolTable.getInGlobal(expression.getType().getName());
				if(auxKraClass != null) {
					if(auxKraClass.getSuperclass() != null){
						if(currentMethod.getType().getCname() != auxKraClass.getSuperclass().getCname()){
							signalError.showError("Type error: type of the expression returned is not subclass of the method return type");

						}
					} else {
						signalError.showError("Type error: type of the expression returned is not subclass of the method return type");
					}
				} 
			}
			//signalError.showError("Type error: type of the expression returned is not subclass of the method return type");
		}

		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();

		return new returnStatement(expression);
	}

	private ReadStatement readStatement() {
		ArrayList<Variable> varList = new ArrayList<Variable>();
		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
		lexer.nextToken();
		while (true) {
			if ( lexer.token == Symbol.THIS ) {
				lexer.nextToken();
				if ( lexer.token != Symbol.DOT ) signalError.showError(". expected");
				lexer.nextToken();
			}
			if ( lexer.token != Symbol.IDENT )
				signalError.show(ErrorSignaller.ident_expected);

			String name = lexer.getStringValue();
			Variable v = symbolTable.getInLocal(name);
			// e se a variavel que a funcao esta tentando usar for uma instancia da classe
			if (v == null) {
				if(currentClass.searchInstanceVariable(name) == null)
					signalError.showError("Variavel nao existe");
			}
			if(v.getType() != Type.stringType && v.getType() != Type.intType) {
				signalError.showError("Command 'read' does not accept '" + v.getType().toString() + "' variables");
			}
			varList.add(v);
			lexer.nextToken();
			if ( lexer.token == Symbol.COMMA )
				lexer.nextToken();
			else
				break;
		}

		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();
		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();

		return new ReadStatement(varList);
	}

	private WriteStatement writeStatement() {

		Expr expr = null;

		ExprList expressionList = new ExprList();
		//System.out.println("AQUI MANO");
		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
		lexer.nextToken();
		expressionList = exprList();
		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();


		Iterator<Expr> it = expressionList.elements();
		//for(Expr e : expressionList.elements()) {
		// para cada elemento dentro dessa lista, verificar se eh do tipo boolena, se for, erro: write nao aceita comando boolean
		while(it.hasNext()) {
			expr = it.next();
			if(expr != null) {
				if(expr.getType() == Type.booleanType) {
					signalError.showError("Command 'write' does not accept 'boolean' expressions");
				}
				if(expr.getType() instanceof KraClass) {
					signalError.showError("Command 'write' does not accept objects");
				}
			}
		}

		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();

		return new WriteStatement(expressionList);
	}

	private WritelnStatement writelnStatement() {

		Expr expr = null;

		ExprList expressionList = new ExprList();

		lexer.nextToken();
		if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
		lexer.nextToken();
		expressionList = exprList();
		if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
		lexer.nextToken();


		Iterator<Expr> it = expressionList.elements();
		//for(Expr e : expressionList.elements()) {
		// para cada elemento dentro dessa lista, verificar se eh do tipo boolena, se for, erro: write nao aceita comando boolean
		while(it.hasNext()) {
			expr = it.next();
			if(expr != null) {
				if(expr.getType() == Type.booleanType) {
					signalError.showError("Command 'write' does not accept 'boolean' expressions");
				}
				if(expr.getType() instanceof KraClass) {
					signalError.showError("Command 'write' does not accept objects");
				}
			}
		}

		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();

		return new WritelnStatement(expressionList);
	}

	private BreakStatement breakStatement() {
		if(flagWhile == 0) {
			signalError.showError("Command 'break' outside a command 'while'");
		} else {
			flagWhile--;
			lexer.nextToken();
			if ( lexer.token != Symbol.SEMICOLON )
				signalError.show(ErrorSignaller.semicolon_expected);
			lexer.nextToken();
		}
		/*lexer.nextToken();
		if ( lexer.token != Symbol.SEMICOLON )
			signalError.show(ErrorSignaller.semicolon_expected);
		lexer.nextToken();*/
		return new BreakStatement();
	}

	private nullStatement nullStatement() {
		lexer.nextToken();
		return new nullStatement();
	}

	private ExprList exprList() {
		// ExpressionList ::= Expression { "," Expression }

		ExprList anExprList = new ExprList();
		anExprList.addElement(expr());
		while (lexer.token == Symbol.COMMA) {
			lexer.nextToken();
			anExprList.addElement(expr());
		}
		return anExprList;
	}

	private Expr expr() {

		Expr right = null;

		Expr left = simpleExpr();
		Symbol op = lexer.token;
		if ( op == Symbol.EQ || op == Symbol.NEQ || op == Symbol.LE
				|| op == Symbol.LT || op == Symbol.GE || op == Symbol.GT ) {
			lexer.nextToken();
			right = simpleExpr();
			
			// se nao forem de tipos basicos, ver as superclasses
			if(left.getType() instanceof KraClass && right.getType() instanceof KraClass) {
				//ver se a classe é subtipo
				Type typeLeft =  left.getType();
				Type typeRight = right.getType();
				boolean flagSubClass = false;
				if(typeLeft.getName().equals(typeRight.getName())) {
					flagSubClass = true;
				} else {
					KraClass classKra = symbolTable.getInGlobal(typeRight.getName());
					KraClass classKra2 = symbolTable.getInGlobal(typeLeft.getName());
					if(classKra != null && classKra2 != null) {
						classKra = classKra.getSuperclass();
						classKra2 = classKra2.getSuperclass();
						while(classKra != null) {
							if(classKra.getName().equals(typeLeft.getName())) {
								flagSubClass = true;
							}
							classKra = classKra.getSuperclass();
						}
						/*porque pode ter
						 * A a;
						 * B b; (B extends A)
						 * 
						 * a == b
						 * b == a  
						 *  
						 */
						while(classKra2 != null) {
							if(classKra2.getName().equals(typeRight.getName())) {
								flagSubClass = true;
							}
							classKra2 = classKra2.getSuperclass();
						}
					}
				}
				if(flagSubClass == false)
					signalError.showError("classes não são subtipos");
			} else {
			
			
				if(!(left.getType().getName().equals(right.getType().getName()))
						&& (op == Symbol.EQ || op == Symbol.NEQ)) {
					signalError.showError("Incompatible types cannot be compared with '" + op + "' because the result will always be 'false'");
				}
			}

			left = new CompositeExpr(left, op, right);
		}


		/*else {
			if(op == Symbol.SEMICOLON) {
				// nao pode ter i; - tem que ter uma instrucao
				signalError.showError("Statement expected");
			}
		}*/
		return left;
	}

	private Expr simpleExpr() {
		Symbol op;
		//System.out.println("tokenzinho: " + lexer.token);
		Expr left = term();
		while ((op = lexer.token) == Symbol.MINUS || op == Symbol.PLUS
				|| op == Symbol.OR) {
			if(left.getType() == Type.booleanType && (op == Symbol.MINUS || op == Symbol.PLUS)) {
				signalError.showError("type boolean does not support operation '" + op + "'");
			}
			lexer.nextToken();
			Expr right = term();
			// tem que ser int + int
			if(op == Symbol.PLUS && left.getType() == Type.intType && right.getType() != Type.intType) {
				signalError.showError("operator '+' of 'int' expects an 'int' value");
			}
			if(op == Symbol.OR && left.getType() == Type.intType && right.getType() == Type.intType) {
				signalError.showError("nao pode ter int com boolean");
			}
			left = new CompositeExpr(left, op, right);
		}
		return left;
	}

	private Expr term() {
		Symbol op;

		Expr left = signalFactor();
		while ((op = lexer.token) == Symbol.DIV || op == Symbol.MULT
				|| op == Symbol.AND) {
			if(left.getType() == Type.intType && op == Symbol.AND) {
				signalError.showError("type 'int' does not support operator '&&'");
			}
			lexer.nextToken();
			Expr right = signalFactor();
			left = new CompositeExpr(left, op, right);
		}
		return left;
	}

	private Expr signalFactor() {
		Symbol op;
		if ( (op = lexer.token) == Symbol.PLUS || op == Symbol.MINUS ) {
			lexer.nextToken();
			Expr exprFac = factor();
			if(exprFac.getType() instanceof KraClass)
				signalError.showError("unario com tipo objeto (instancia classe)");
			if(exprFac.getType() == Type.booleanType)
				signalError.showError("unario nao pode ser com boolean");
			return new SignalExpr(op, exprFac);
		}
		else
			return factor();
	}

	/*
	 * Factor ::= BasicValue | "(" Expression ")" | "!" Factor | "null" |
	 *      ObjectCreation | PrimaryExpr
	 *
	 * BasicValue ::= IntValue | BooleanValue | StringValue
	 * BooleanValue ::=  "true" | "false"
	 * ObjectCreation ::= "new" Id "(" ")"
	 * PrimaryExpr ::= "super" "." Id "(" [ ExpressionList ] ")"  |
	 *                 Id  |
	 *                 Id "." Id |
	 *                 Id "." Id "(" [ ExpressionList ] ")" |
	 *                 Id "." Id "." Id "(" [ ExpressionList ] ")" |
	 *                 "this" |
	 *                 "this" "." Id |
	 *                 "this" "." Id "(" [ ExpressionList ] ")"  |
	 *                 "this" "." Id "." Id "(" [ ExpressionList ] ")"
	 */
	private Expr factor() {
		//System.out.println(lexer.getStringValue());
		Expr anExpr;
		ExprList exprList = null;
		Expr retorno = null;
		String messageName, id;
		//System.out.println("token token token: " + lexer.token);
		switch (lexer.token) {
			// IntValue.real
			case LITERALINT:
				return literalInt();
				// BooleanValue
			case FALSE:
				lexer.nextToken();
				return LiteralBoolean.False;
				// BooleanValue
			case TRUE:
				lexer.nextToken();
				return LiteralBoolean.True;
				// StringValue
			case LITERALSTRING:
				String literalString = lexer.getLiteralStringValue();
				lexer.nextToken();
				return new LiteralString(literalString);
				// "(" Expression ")" |
			case LEFTPAR:
				lexer.nextToken();
				anExpr = expr();
				if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
				lexer.nextToken();
				return new ParenthesisExpr(anExpr);
	
				// "null"
			case NULL:
				lexer.nextToken();
				return new NullExpr();
				// "!" Factor
			case NOT:
				lexer.nextToken();
				anExpr = expr();
				if(anExpr.getType() == Type.intType) {
					signalError.showError("Operator '!' does not accepts 'int' values");
				}
				if(anExpr.getType() instanceof KraClass)
					signalError.showError("'!' nao pode ser usado com objeto");
				return new UnaryExpr(anExpr, Symbol.NOT);
				// ObjectCreation ::= "new" Id "(" ")"
			case NEW:
				lexer.nextToken();
				if ( lexer.token != Symbol.IDENT )
					signalError.showError("Identifier expected");
	
				String className = lexer.getStringValue();
				/*
				 * // encontre a classe className in symbol table KraClass
				 *      aClass = symbolTable.getInGlobal(className);
				 *      if ( aClass == null ) ...
				 */
				KraClass aClass = symbolTable.getInGlobal(className);
				if (aClass == null)
					signalError.showError("Class '" + className + "' was not found");
	
				lexer.nextToken();
				if ( lexer.token != Symbol.LEFTPAR ) signalError.showError("( expected");
				lexer.nextToken();
				if ( lexer.token != Symbol.RIGHTPAR ) signalError.showError(") expected");
				lexer.nextToken();
				/*
				 * return an object representing the creation of an object
				 */
				return new NewObject(aClass);
				/*Variable va = new InstanceVariable(className, aClass);
				return new VariableExpr(va);*/
				//return null;
				/*
	          	 * PrimaryExpr ::= "super" "." Id "(" [ ExpressionList ] ")"  |
	          	 *                 Id  |
	          	 *                 Id "." Id |
	          	 *                 Id "." Id "(" [ ExpressionList ] ")" |
	          	 *                 Id "." Id "." Id "(" [ ExpressionList ] ")" |
	          	 *                 "this" |
	          	 *                 "this" "." Id |
	          	 *                 "this" "." Id "(" [ ExpressionList ] ")"  |
	          	 *                 "this" "." Id "." Id "(" [ ExpressionList ] ")"
				 */
			case SUPER:
				// "super" "." Id "(" [ ExpressionList ] ")"
				if(currentClass.getName().equals("Program"))
					signalError.showError("'super' used in class 'Program' that does not have a superclass");
				
				if(currentClass.getSuperclass() == null)
					signalError.showError("Class doesn't have a superclass");
					
				lexer.nextToken();
				if ( lexer.token != Symbol.DOT ) {
					signalError.showError("'.' expected");
				}
				else
					lexer.nextToken();
				if ( lexer.token != Symbol.IDENT )
					signalError.showError("Identifier expected");
				messageName = lexer.getStringValue();
	
				/*
				 * para fazer as conferências semânticas, procure por 'messageName'
				 * na superclasse/superclasse da superclasse etc
				 */
				KraClass currentC = currentClass.getSuperclass();
				MethodDec m = null;
				while(currentC != null) {
					m = currentC.getMethod(messageName);
					if(m == null || m.getQualifier() == Symbol.PRIVATE)
						currentC = currentC.getSuperclass();
					else
						break;
				}
				if(m == null || m.getQualifier() == Symbol.PRIVATE)
					signalError.showError("Não achei o método público em nenhuma superclasse");
	
				lexer.nextToken();
				exprList = realParameters();
	
				ast.KraClass superclasse = this.currentClass.getSuperclass();
				MethodDec superMeth = superclasse.getMethod(messageName);
				if (superclasse != null){
					if(superMeth == null) {
						superclasse = superclasse.getSuperclass();
						while(superclasse != null) {
							superMeth = superclasse.getMethod(messageName);
							if(superMeth == null)
								superclasse = superclasse.getSuperclass();
							else {
								if(superMeth.getSizeParamList() == exprList.getSize()) {
									Iterator<Variable> itSuperMeth = superMeth.getParamList().elements();
									Iterator<Expr> itExprList = exprList.elements();
									while(itSuperMeth.hasNext() && itExprList.hasNext()) {
										Variable vSuperMeth = itSuperMeth.next();
										Expr exprL = itExprList.next();
										if(vSuperMeth.getType() == Type.intType ||
												vSuperMeth.getType() == Type.stringType ||
												vSuperMeth.getType() == Type.booleanType) {
											if(!(vSuperMeth.getType().equals(exprL.getType()))) {
												signalError.showError("tipos não compatíveis");
											}
										} else {
											if(vSuperMeth.getType() == Type.undefinedType || exprL.getType() == Type.undefinedType) {
												signalError.showError("tipos undefined");
											}
	
											//ver se a classe é subtipo
											Type typeSuper =  vSuperMeth.getType();
											Type typeExprL = exprL.getType();
											boolean flagSubClass = false;
											if(typeSuper.getName().equals(typeExprL.getName())) {
												flagSubClass = true;
											} else {
												KraClass classKra = symbolTable.getInGlobal(typeExprL.getName());
												if(classKra != null) {
													classKra = classKra.getSuperclass();
													while(classKra != null) {
														if(classKra.getName().equals(typeSuper.getName())) {
															flagSubClass = true;
														}
														classKra = classKra.getSuperclass();
													}
												}
											}
											if(!flagSubClass)
												signalError.showError("classes não são subtipos");
										}
									}
								} else
									signalError.showError("tamanho da lista de parametros é diferente");
							}
	
						}
					}
					//procura na publica
					//confere se a qntd de parametros é igual e se os tipos sao COMPATIVEIS
				}
	
				/*lexer.nextToken();
				exprList = realParameters();*/
				// checkRealParam(method, exprList);
				return new MessageSendToSuper(m, exprList, currentC);
	
				//break;
			case IDENT:
				/*
	          	 * PrimaryExpr ::=
	          	 *                 Id  |
	          	 *                 Id "." Id |
	          	 *                 Id "." Id "(" [ ExpressionList ] ")" |
	          	 *                 Id "." Id "." Id "(" [ ExpressionList ] ")" |
				 */
	
				String firstId = lexer.getStringValue();
				lexer.nextToken();
				if ( lexer.token != Symbol.DOT ) {
					if (lexer.token == Symbol.IDENT)
						signalError.showError("Type '" + firstId + "' was not found");
					// Id
					// retorne um objeto da ASA que representa um identificador
					Variable v = null;
					if(lexer.token == Symbol.LEFTPAR) {
						signalError.showError("'.' or '=' expected after identifier");
					}
	
					v = symbolTable.getInLocal(firstId);
					//testar v null
					if(v == null) {
						v = currentClass.searchInstanceVariable(firstId);
						if(v != null) {
							signalError.showError("Instance variable did not use the keyword 'this'");
						}
						/*InstanceVariable instV = currentClass.searchInstanceVariable(lexer.getStringValue());
						if(instV == null) {
							signalError.showError("Nao existe essa variavel, queridinho");
						}*/
					}
					if(v == null) {
						signalError.showError("Can't find variable");
					}
					return new VariableExpr(v, false);
					//return null;
				}
				else { // Id "."
					lexer.nextToken(); // coma o "."
					if ( lexer.token != Symbol.IDENT ) {
						signalError.showError("Identifier expected");
					}
					else {
						// Id "." Id
						lexer.nextToken();
						id = lexer.getStringValue();
						
						//lexer.nextToken();
						//System.out.println("hello: " + lexer.token);
						if ( lexer.token == Symbol.DOT ) {
							// Id "." Id "." Id "(" [ ExpressionList ] ")"
							/*
							 * se o compilador permite variáveis estáticas, é possível
							 * ter esta opção, como
							 *     Clock.currentDay.setDay(12);
							 * Contudo, se variáveis estáticas não estiver nas especificações,
							 * sinalize um erro neste ponto.
							 */
	
							lexer.nextToken();
							if ( lexer.token != Symbol.IDENT )
								signalError.showError("Identifier expected");
							messageName = lexer.getStringValue();
	
							lexer.nextToken();
							exprList = this.realParameters();
	
							//return new MessageSendToVariable(exprList);
	
						}
						else if ( lexer.token == Symbol.LEFTPAR ) {
							
							// Id "." Id "(" [ ExpressionList ] ")"
							exprList = this.realParameters();
							/*
							 * para fazer as conferências semânticas, procure por
							 * método 'ident' na classe de 'firstId'
							 */
							// primeiro vejo se existe a variavel utilizada
							Variable v = symbolTable.getInLocal(firstId);
							// pode ser uma variavel ou uma variavel de instancia
							Variable InstV = currentClass.searchInstanceVariable(firstId);
							MethodDec auxMeth = null;
							if(v != null || InstV != null) {
								if(v == null)
									v = InstV;
								//se o tipo não for uma classe, não posso chamar objeto
								if(!isType(v.getType().getName()))
									signalError.showError("MUDAR ESTE ERROR. The first identifier must be a class type to call methods");
	
	
							//if(v != null) {
								//ver se o tipo é uma classe
								if(v.getType() instanceof KraClass) {
									//nao existe metodo na classe da variavel
									KraClass auxKra = symbolTable.getInGlobal(v.getType().getName());
									auxMeth = auxKra.getMethod(id);
									if(auxMeth == null) {
										// verificar se existe nas superclasses
										auxKra = auxKra.getSuperclass();
										while(auxKra != null) {
											auxMeth = auxKra.getMethod(id);
											if(auxMeth == null) {
												auxKra = auxKra.getSuperclass();
											} else
												break;
										}
	
									}
									if(auxMeth == null) {
										signalError.showError("Method '" + id + "' was not found in class '" + firstId + "' or its superclasses");
									} else if(auxMeth.getQualifier() == Symbol.PRIVATE)
										signalError.showError("Can't call private method from class '" + firstId + "'");
								}
							}
							return new MessageSendToVariable(v, auxMeth, exprList, currentClass);
	
						}
						else {
							// retorne o objeto da ASA que representa Id "." Id
							KraClass classKra = symbolTable.getInGlobal(firstId);
							if(classKra == null)
								signalError.showError("Identifier is not a class");
							return new MessageSendToVariable(classKra);
	
						}
					}
				}
				break;
			case THIS:
				/*
				 * Este 'case THIS:' trata os seguintes casos:
	          	 * PrimaryExpr ::=
	          	 *                 "this" |
	          	 *                 "this" "." Id |
	          	 *                 "this" "." Id "(" [ ExpressionList ] ")"  |
	          	 *                 "this" "." Id "." Id "(" [ ExpressionList ] ")"
				 */
				lexer.nextToken();
				//Expr retorno = null;
				if ( lexer.token != Symbol.DOT ) {
					// only 'this'
					// retorne um objeto da ASA que representa 'this'
					// confira se não estamos em um método estático
					//return null;
					return new ThisExpr(currentClass);
				}
				else {
					lexer.nextToken();
					if ( lexer.token != Symbol.IDENT )
						signalError.showError("Identifier expected");
					id = lexer.getStringValue();
					lexer.nextToken();
					// já analisou "this" "." Id
	
					//Expr retorno = null;
					if ( lexer.token == Symbol.LEFTPAR ) {
						// "this" "." Id "(" [ ExpressionList ] ")"
						/*
						 * Confira se a classe corrente possui um método cujo nome é
						 * 'ident' e que pode tomar os parâmetros de ExpressionList
						 */
						MethodDec meth = currentClass.getMethod(id);
						if(meth == null) {
							KraClass auxKraClass = currentClass.getSuperclass();
							while(auxKraClass != null) {
								meth = auxKraClass.getMethod(id);
								if(meth == null || meth.getQualifier() == Symbol.PRIVATE )
									auxKraClass = auxKraClass.getSuperclass();
								else {
									break;
								}
							}
	
						}
	
						if(meth == null)
							signalError.showError("nao encontrei o método");
	
						exprList = this.realParameters();
						if ( exprList == null ) {
							if ( meth.getParamList() != null && meth.getSizeParamList() != 0 )
								signalError.showError("Method tal e tal deveria ter parametros");
						}
						else 
							if(meth.getSizeParamList() != exprList.getSize())
							    signalError.showError("tamanhos diferentes da lista de parametros");
						if(exprList != null) {
						
							Iterator<Variable> itMeth = meth.getParamList().elements();
							Iterator<Expr> itE = exprList.elements();
							while(itMeth.hasNext()){
								Variable v = itMeth.next();
								Expr ex = itE.next();
								if (isType(ex.getType().getName())) {
									//ver se a classe é subtipo
									Type typeSuper =  v.getType();
									Type typeExprL = ex.getType();
									boolean flagSubClass = false;
									if (typeSuper.getName().equals(typeExprL.getName())) {
										flagSubClass = true;
									} else {
										KraClass classKra = (KraClass ) typeExprL; // symbolTable.getInGlobal(typeExprL.getName());
										if(classKra != null) {
											classKra = classKra.getSuperclass();
											while(classKra != null) {
												if(classKra.getName().equals(typeSuper.getName())) {
													flagSubClass = true;
												}
												classKra = classKra.getSuperclass();
											}
										}
									}
									if(!flagSubClass)
										signalError.showError("classes não são subtipos");
								} 
							    else {
									if(ex.getType() != v.getType())
										signalError.showError("metodo tem tipo diferente");
								}
							}
						}
						return new MessageSendToSelf(currentClass, meth, exprList);
					}
					else if ( lexer.token == Symbol.DOT ) {
						// "this" "." Id "." Id "(" [ ExpressionList ] ")"
						lexer.nextToken();
						if ( lexer.token != Symbol.IDENT )
							signalError.showError("Identifier expected");	
						//System.out.println("antes disso");
						String message = lexer.getStringValue();
						MethodDec meth = null;
						Variable v = currentClass.searchInstanceVariable(id);
						if(v != null){
							if(isType(v.getType().getName())){
								KraClass currentCl = symbolTable.getInGlobal(v.getType().getName());
								if(currentCl != null) {
									meth = currentCl.getMethod(message);
									if(meth == null){
										currentCl = currentCl.getSuperclass();
										while(currentCl != null){
											meth = currentCl.getMethod(message);
											if(meth == null || meth.getQualifier() == Symbol.PRIVATE)
												currentCl = currentCl.getSuperclass();
											else
												break;
										}
									} else {
										//System.out.println("existo");
										lexer.nextToken();
									}
								}
							} else {
								signalError.showError("essa variavel não pode chamar metodos");
							}
						} else {
							signalError.showError("nenhuma instancia encontrada");
						}
	
						if(meth == null)
							signalError.showError("não achei o método");
						lexer.nextToken();
						
						
						if (lexer.token != Symbol.RIGHTPAR) {
							//lexer.nextToken();//Talvez dê errado futuramente
							//System.out.println(lexer.token + " Estoy aki ");
							
							exprList = realParameters();
							//System.out.println("Passei");
							if(exprList != null){
								if(meth.getSizeParamList() != exprList.getSize())
									signalError.showError("tamanho da lista de parametros é diferente");
								Iterator<Variable> itMeth = meth.getParamList().elements();
								Iterator<Expr> iE = exprList.elements();
								while(itMeth.hasNext()){
									Variable var = itMeth.next();
									Expr ex = iE.next();
									if(isType(ex.getType().getName())){
										//ver se a classe é subtipo
										Type typeSuper =  v.getType();
										Type typeExprL = ex.getType();
										boolean flagSubClass = false;
										if(typeSuper.getName().equals(typeExprL.getName())) {
											flagSubClass = true;
										} else {
											KraClass classKra = symbolTable.getInGlobal(typeExprL.getName());
											if(classKra != null) {
												classKra = classKra.getSuperclass();
												while(classKra != null) {
													if(classKra.getName().equals(typeSuper.getName())) {
														flagSubClass = true;
													}
													classKra = classKra.getSuperclass();
												}
											}
										}
										if(!flagSubClass)
											signalError.showError("classes não são subtipos");
										//boolean check = checkClassType(var.getType(), ex.getType());
										//if(!check)
											//signalError.showError("method type mismatch");
									} else {
										if(ex.getType().getName().equals(var.getType().getName()))
											signalError.showError("tipos diferentes dos métodos");
									}
								}
							}
							
							retorno = new MessageSendToSelf(currentClass, v, meth, exprList);
						}
						//ver se ja deu fechaar
						if(lexer.token != Symbol.RIGHTPAR) 
							signalError.showError("Falta fechar )");
						//System.out.println("tocar " + lexer.token);
						lexer.nextToken();
						//System.out.println("depois disso");
						
						return retorno;
						
					}
					else {
						// retorne o objeto da ASA que representa "this" "." Id
						/*
						 * confira se a classe corrente realmente possui uma
						 * variável de instância 'ident'
						 */
						Variable v = currentClass.searchInstanceVariable(id);
						if(v == null)
							signalError.showError("There is no instance " + id + " in class " + currentClass.getName());
						//return new VariableExpr(v, true);
						return new MessageSendStatement(v, currentClass);
	
						//return null;
					}
				}
				//break;
			default:
				signalError.showError("Expression expected");
		}
		return null;
	}

	private LiteralInt literalInt() {

		LiteralInt e = null;

		// the number value is stored in lexer.getToken().value as an object of
		// Integer.
		// Method intValue returns that value as an value of type int.
		int value = lexer.getNumberValue();
		lexer.nextToken();
		return new LiteralInt(value);
	}

	private static boolean startExpr(Symbol token) {

		return token == Symbol.FALSE || token == Symbol.TRUE
				|| token == Symbol.NOT || token == Symbol.THIS
				|| token == Symbol.LITERALINT || token == Symbol.SUPER
				|| token == Symbol.LEFTPAR || token == Symbol.NULL
				|| token == Symbol.IDENT || token == Symbol.LITERALSTRING;

	}

	private doWhileStatement doWhileStatement() {

		Statement compositeList;

		lexer.nextToken();
		if(lexer.token != Symbol.LEFTCURBRACKET) {
			signalError.showError("'{' expected");
		}
		compositeList = compositeStatement();
		//lexer.nextToken();
		//System.out.println("dowhile: " + lexer.token);
		if(lexer.token != Symbol.WHILE) {
			signalError.showError("'while' expected");
		}
		lexer.nextToken();
		if(lexer.token != Symbol.LEFTPAR) {
			signalError.showError("'(' expected after 'while'");
		}
		lexer.nextToken();
		Expr expression = expr();
		if(expression.getType() != Type.booleanType) {
			signalError.showError("non-boolean expression in 'while' command");
		}
		if(lexer.token != Symbol.RIGHTPAR) {
			signalError.showError("')' expected");
		}
		lexer.nextToken();
		if(lexer.token != Symbol.SEMICOLON) {
			signalError.show(ErrorSignaller.semicolon_expected);
			//signalError.showError("';' expected");
		}
		lexer.nextToken();

		return new doWhileStatement(compositeList, expression);
	}

	private SymbolTable		symbolTable;
	private Lexer			lexer;
	private ErrorSignaller	signalError;
	public int flagWhile = 0;
	private MethodDec currentMethod;

}
